#include "Validator.h"

/*
Checks if pass is valid under these requiements:
-must have at least 4 chars
-must include a decimal number
-must include at least one upper case char and one lower case.
*/
bool Validator::isPasswordValid(std::string pass)
{
	bool hasUpper = false;
	bool hasLower = false;
	if (pass.length() > 3)
	{
		if (pass.find(' ') == std::string::npos)
		{
			if (pass.find('0') != std::string::npos ||
				pass.find('1') != std::string::npos ||
				pass.find('2') != std::string::npos ||
				pass.find('3') != std::string::npos ||
				pass.find('4') != std::string::npos ||
				pass.find('5') != std::string::npos ||
				pass.find('6') != std::string::npos ||
				pass.find('7') != std::string::npos ||
				pass.find('8') != std::string::npos ||
				pass.find('9') != std::string::npos)
			{
				for (int i = 0; i < pass.length(); i++)
				{
					if (pass[i] >= 'A' && pass[i] <= 'Z')
					{
						hasUpper = true;
					}
					else if (pass[i] >= 'a' && pass[i] <= 'z')
					{
						hasLower = true;
					}
					if (hasLower && hasUpper)
					{
						//pass is valid
						return true;
					}
				}
			}
		}
		
	}
	return false;
}

/*
Checks if username is valid under these requiements:
-must have at least one char
-first char must be an english char(upper case || lower case).
-mustn't have a blank space in it.
*/
bool Validator::isUsernameValid(std::string username)
{
	if (username.length() > 0)
	{
		if ((username[0] >= 'A' && username[0] <= 'Z') || (username[0] >= 'a' && username[0] <= 'z'))
		{
			if (username.find(' ') == std::string::npos)
			{
				//username is valid
				return true;
			}
		}
	}
	return false;
}
