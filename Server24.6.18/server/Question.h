#pragma once

#include <string>
#include <stdlib.h>
#include <time.h>

class Question
{
private:
	std::string _question;
	std::string _answers[4];
	int _ansArrLength;
	int _correctAnswerIndex;
	int _id;

public:
	Question(int, std::string, std::string, std::string, std::string, std::string);
	~Question();
	std::string getQuestion();
	std::string* getAnswers();
	int getCorrectAnswerIndex();
	int getArrLength();
	int getID();
};
