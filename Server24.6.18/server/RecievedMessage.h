#pragma once
#include "Helper.h"
#include "User.h"

class RecievedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<std::string> _values;

public:
	RecievedMessage(SOCKET sock, int code);
	RecievedMessage(SOCKET sock, int code, std::vector<std::string> values);
	SOCKET getSock();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	std::vector<std::string>& getValues();

};
