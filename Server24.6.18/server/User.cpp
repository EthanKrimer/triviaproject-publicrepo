#include "User.h"

User::User(std::string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
	this->_currGame = nullptr;
	this->_currRoom = nullptr;
}

void User::send(std::string message)
{
	Helper::sendData(this->_sock, message);
}

std::string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room * User::getRoom()
{
	return this->_currRoom;
}

Game * User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* gm)
{
	this->_currRoom = nullptr;
	this->_currGame = gm;
}

void User::clearGame()
{
	this->_currGame = nullptr;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionNo, int questionTime)
{
	if (this->_currRoom == nullptr)
	{
		this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionNo, questionTime);
		this->send(std::to_string(Protocol::ServerClient::createRoomSuccess));
		return 1;
	}
	this->send(std::to_string(Protocol::ServerClient::createRoomFail));
	return 0;//inverted because of protocol
}

bool User::joinRoom(Room * newRoom)
{
	bool joined = false;
	if (this->_currRoom == nullptr)
	{
		if (newRoom->joinRoom(this))
		{
			this->_currRoom = newRoom;
			joined = true;
		}
	}
	return joined;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->clearRoom();
	}
}

int User::closeRoom()
{
	int roomId = -1;
	if (this->_currRoom != nullptr)
	{
		roomId = this->_currRoom->closeRoom(this);
		if (roomId != -1)
		{
			delete this->_currRoom;
			this->clearRoom();
		}
	}

	return roomId;//-1 returned if room didn't close
}

bool User::leaveGame()
{
	bool gameStillOn = false;
	if (this->_currGame != nullptr)
	{
		gameStillOn = this->_currGame->leaveGame(this);
		this->clearGame();
	}
	return gameStillOn;
}
