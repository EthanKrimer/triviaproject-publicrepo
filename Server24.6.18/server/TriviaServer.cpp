#include "TriviaServer.h"

#define PassIndexInRcvMsgVal 1
#define UserIndexInRcvMsgVal 0
#define EmailIndexInRcvMsgVal 2

#define AnsNoIndexInRcvMsgVal 0
#define TimeIndexInRcvMsgVal 1

#define RoomNameIndexInRcvMsgVal 0
#define MaxUsersIndexInRcvMsgVal 1
#define QuestionNumIndexInRcvMsgVal 2
#define QuestionTimeIndexInRcvMsgVal 3

#define RoomIdIndexInRcvMsgVal 0

#define ansNumIndexInRcvMsgVal 0
#define ansTimeIndexInRcvMsgVal 1

int TriviaServer::_roomIdSequence = 0;//static roomIdSequence

/*
cto'r
*/
TriviaServer::TriviaServer()
{
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
dto'r
*/
TriviaServer::~TriviaServer()
{
	this->_roomsList.clear();
	auto it = this->_connectedUsers.begin();
	for(it; it != this->_connectedUsers.end(); it++)
	{
	::closesocket(it->first);
	}
	this->_connectedUsers.clear();
	::closesocket(this->_socket);
}

/*
binds and starts listening(using TriviaServer::bindAndListen)
creates a thread for handling received msgs
accepts clients using TriviaServer::accept and creates a thread for clientHandle for each client
*/
void TriviaServer::server()
{
	bindAndListen();
	thread rcvMsgT(&TriviaServer::handleRecivedMessages, this);
	rcvMsgT.detach();
	while (true)
	{
		try
		{
			this->accept();
		}
		catch (const std::exception& e)
		{
			cout << e.what() << endl;
		}
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(LISTENING_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << LISTENING_PORT << endl;
}

/*
creates a thread to handle client after it accepts it
*/
void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	cout << "Client" << client_socket << " accepted. Server and client can speak" << endl;
	thread clientHandle(&TriviaServer::clientHandler, this, client_socket);
	clientHandle.detach();
}

void TriviaServer::clientHandler(SOCKET clientSocket)
{
	try
	{
		int code = Helper::getMessageTypeCode(clientSocket);
		while (true)
		{
			addRecievedMessage(buildRecievedMessage(clientSocket, code));
			if (code == Protocol::ClientServer::leaveApp)
			{
				return;
			}
			code = Helper::getMessageTypeCode(clientSocket);
		}
		//string endGame = "121" ;
		//(buildRecievedMessage(clientSocket, code));
	}
	catch (exception & e)
	{
		cout << "client quit or exception in TriviaServer::clientHandler: " << e.what() << endl;
		addRecievedMessage(buildRecievedMessage(clientSocket, Protocol::ClientServer::self::leaveApp));
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage* rcvMsg)
{
	if (rcvMsg != nullptr && rcvMsg->getUser() != nullptr)
	{
		try
		{
			if (rcvMsg->getUser()->getRoom())
			{
				_roomsList.erase(rcvMsg->getUser()->getRoom()->getId());
			}
			SOCKET userSock = rcvMsg->getSock();
			handleSignout(rcvMsg);
			::closesocket(userSock);
		}
		catch (exception & e)
		{
			cout << "Exception in TriviaServer::safeDeleteUser: " << e.what() << endl;
		}
	}
}

User * TriviaServer::handleSignin(RecievedMessage * rcvMsg)
{
	try
	{
		std::vector<std::string> val = rcvMsg->getValues();
		rcvMsg->setUser(new User(val[UserIndexInRcvMsgVal], rcvMsg->getSock()));
		if (this->_db.isUserAndPassMatch(val[UserIndexInRcvMsgVal], val[PassIndexInRcvMsgVal]))
		{
			if (getUserByName(val[UserIndexInRcvMsgVal]))
			{
				//fail- already signed in
				rcvMsg->getUser()->send(std::to_string(Protocol::ServerClient::SignInServerFeedback::UserIsAlreadyConnected));
			}
			else
			{
				this->_connectedUsers.insert(std::pair<SOCKET, User*>(rcvMsg->getSock(), rcvMsg->getUser()));
				rcvMsg->getUser()->send(std::to_string(Protocol::ServerClient::SignInServerFeedback::Success));

			}
		}
		else
		{
			rcvMsg->getUser()->send(std::to_string(Protocol::ServerClient::SignInServerFeedback::WrongDetails));
		}
	}
	catch (exception& e)
	{
		cout << "Exception in TriviaServer::handleSignin: " << e.what() << endl;
	}

	return rcvMsg->getUser();
}

bool TriviaServer::handleSignUp(RecievedMessage * rcvMsg)
{
	try
	{
		vector <std::string> val = rcvMsg->getValues();
		User* user = new User(val[UserIndexInRcvMsgVal], rcvMsg->getSock());
		rcvMsg->setUser(user);
		if (!Validator::isPasswordValid(val[PassIndexInRcvMsgVal]))
			user->send(std::to_string(Protocol::ServerClient::SignUpServerFeedback::PassIllegal));
		else
		{
			if (!Validator::isUsernameValid(val[UserIndexInRcvMsgVal]))
				user->send(std::to_string(Protocol::ServerClient::SignUpServerFeedback::UsernameIllegal));
			else
			{
				if (this->_db.isUserExists(val[UserIndexInRcvMsgVal]))
					user->send(std::to_string(Protocol::ServerClient::SignUpServerFeedback::UsernameAlreadyExist));
				else
				{
					if (this->_db.addNewUser(val[UserIndexInRcvMsgVal], val[PassIndexInRcvMsgVal], val[EmailIndexInRcvMsgVal]))
					{
						user->send(std::to_string(Protocol::ServerClient::SignUpServerFeedback::SignUpSuccess));
						cout << "user '" << user->getUsername() << "' has been signed up" << endl;
						return true;
					}
					else
						user->send(std::to_string(Protocol::ServerClient::SignUpServerFeedback::other));
					cout << "user could not be signed up" << endl;
				}
			}
		}
	}
	catch (exception & e)
	{
		cout << "Exception in TriviaServer::handleSignUp: " << e.what() << endl;
		return false;
	}

	return false;
}

void TriviaServer::handleSignout(RecievedMessage* rcvMsg)
{
	if (rcvMsg->getUser())
	{
		auto it = this->_connectedUsers.find(rcvMsg->getSock());
		if (it != this->_connectedUsers.end())
		{
			this->_connectedUsers.erase(rcvMsg->getSock());
		}
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage * rcvMsg)
{
	Game* usersGame = rcvMsg->getUser()->getGame();
	if (!rcvMsg->getUser()->leaveGame())//if no players in game, delete the game
		delete usersGame;
}

// check again
void TriviaServer::handleStartGame(RecievedMessage * rcvMsg)
{
	User* curr = nullptr;
	try
	{
		curr = rcvMsg->getUser();
		Room* room = curr->getRoom();
		Game* game = new Game(room->getUsers(), room->getQuestionsNo(), this->_db);
		if (game)
		{
			this->_roomsList.erase(room->getId());
			curr->clearRoom();
			game->sendFirstQuestion();
		}
	}
	catch (exception& e)
	{
		cout << "Exception in TriviaServer::handleStartGame: " << e.what() << endl;
		if (curr)
		{
			curr->send(std::to_string(Protocol::ServerClient::QuestionsAndAnswers::sendQuesFail));
		}
	}

}

void TriviaServer::handlePlayerAnswer(RecievedMessage * rcvMsg)
{
	try
	{
		Game* game = rcvMsg->getUser()->getGame();
		vector<std::string> val = rcvMsg->getValues();
		if (game)
		{
			if (!game->handleAnswerFromUser(rcvMsg->getUser(), atoi(val[AnsNoIndexInRcvMsgVal].c_str()), atoi(val[TimeIndexInRcvMsgVal].c_str())))
			{
				delete game;
			}
		}
	}
	catch (exception& e)
	{
		cout << "excetion in TriviaServer::handlePlayerAnswer: " << e.what() << endl;
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage * rcvMsg)
{
	User* user = rcvMsg->getUser();
	if (user)
	{
		try
		{
			vector<std::string> val = rcvMsg->getValues();
			_roomIdSequence++;
			if (user->createRoom(_roomIdSequence, val[RoomNameIndexInRcvMsgVal], atoi(val[MaxUsersIndexInRcvMsgVal].c_str()),
				atoi(val[QuestionTimeIndexInRcvMsgVal].c_str()), atoi(val[QuestionNumIndexInRcvMsgVal].c_str())))
			{
				this->_roomsList.insert(pair<int, Room*>(_roomIdSequence, user->getRoom()));
				return true;
			}
		}
		catch (exception & e)
		{
			cout << "exception in TriviaServer::handleCreateRoom: " << e.what() << endl;
		}
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage * rcvMsg)
{
	try
	{
		Room* usersRoom = rcvMsg->getUser()->getRoom();
		if (usersRoom)
		{
			auto it = this->_roomsList.find(usersRoom->getId());
			if (rcvMsg->getUser()->closeRoom() != -1)
			{
				if (it != this->_roomsList.end())
				{
					this->_roomsList.erase(it);
					return true;
				}
			}
		}
		return false;
	}
	catch (exception & e)
	{
		cout << "exception TriviaServer::handleCloseRoom: " << e.what() << endl;
		return false;
	}

}

bool TriviaServer::handleJoinRoom(RecievedMessage * rcvMsg)
{
	try
	{
		User* user = rcvMsg->getUser();
		if (user)
		{
			int roomID = atoi(rcvMsg->getValues()[RoomIdIndexInRcvMsgVal].c_str());
			Room* room = getRoomById(roomID);
			if (room)
				return user->joinRoom(room);
			user->send(std::to_string(Protocol::ServerClient::failedRoomDoesntExistOrOtherReason));
		}
		return false;
	}
	catch (exception & e)
	{
		cout << "exception in TriviaServer::handleJoinRoom: " << e.what() << endl;
		return false;
	}
}

bool TriviaServer::handleLeaveRoom(RecievedMessage * rcvMsg)
{
	if (rcvMsg->getUser())
	{
		if (rcvMsg->getUser()->getRoom())
		{
			rcvMsg->getUser()->leaveRoom();
			return true;
		}
	}
	return false;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage * rcvMsg)
{
	try
	{
		User* user = rcvMsg->getUser();
		if (user)
		{
			int roomID = atoi(rcvMsg->getValues()[RoomIdIndexInRcvMsgVal].c_str());
			Room* room = getRoomById(roomID);
			if (room)
			{
				user->send(room->getUsersListMessage());
				return;
			}
			user->send(std::to_string(Protocol::ServerClient::room::failToSendListOfUsers));
		}
	}
	catch (exception& e)
	{
		cout << "exception in TriviaServer::handleGetUsersInRoom: " << e.what() << endl;
	}
}

void TriviaServer::handleGetRooms(RecievedMessage * rcvMsg)
{
	try
	{
		User* user = rcvMsg->getUser();
		if (user)
		{
			std::string s = std::to_string(Protocol::ServerClient::room::sendRoomsListGE) +
				Helper::getPaddedNumber(this->_roomsList.size(), 4);
			auto it = this->_roomsList.begin();
			while (it != this->_roomsList.end())
			{
				std::string name = it->second->getName();
				s += Helper::getPaddedNumber(it->first, 4);
				s += Helper::getPaddedNumber(name.length(), 2);
				s += name;
				it++;
			}
			user->send(s);
		}

	}
	catch (exception & e)
	{
		cout << "exception in TriviaServer::handleGetRooms" << e.what() << endl;
	}
}

void TriviaServer::handleGetBestScores(RecievedMessage * rcvMsg)
{
	try
	{
		vector<pair<string, int>> bestSc = this->_db.getBestScores();
		std::string ret = std::to_string(Protocol::ServerClient::bestScores::bestScoresGE);
		int count = 0;
		for (auto it = bestSc.begin(); it != bestSc.end(); it++)
		{
			ret += Helper::getPaddedNumber(it->first.length(), 2);
			ret += it->first;
			ret += Helper::getPaddedNumber(it->second, 6);
			count++;
		}
		User* curr = rcvMsg->getUser();
		if (count < 3)
		{
			ret += Helper::getPaddedNumber(0, 2);
			ret += Helper::getPaddedNumber(0, 6);
		}
		if (curr)
		{
			curr->send(ret);
		}
	}
	catch (exception& e)
	{
		cout << "Exception in TriviaServer::handleGetBestScores: " << e.what() << endl;
	}
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * rcvMsg)
{
	try
	{
		vector<std::string> vec = this->_db.getPersonalStatus(rcvMsg->getUser()->getUsername());
		if (vec[0] == "0")
		{
			vec[2] = "0";
			vec[3] = "0";
		}
		std::string ret = std::to_string(Protocol::ServerClient::selfStatusGE) + Helper::getPaddedNumber(atoi(vec[0].c_str()), 4)
			+ Helper::getPaddedNumber(atoi(vec[1].c_str()), 6) + Helper::getPaddedNumber(atoi(vec[2].c_str()), 6)
			+ Helper::getPaddedNumber(atoi(vec[3].c_str()), 4);
		rcvMsg->getUser()->send(ret);
	}
	catch (exception& e)
	{
		cout << "Exception in TriviaServer::handleGetPersonalStatus: " << e.what() << endl;
	}
}

void TriviaServer::handleRecivedMessages()
{
	while (true)
	{
		RecievedMessage* curr = nullptr;
		try
		{
			std::unique_lock<std::mutex> locker(this->_mtxRecieveMessages);//lock critical section
			if (this->_queRcvMessages.empty())
			{
				this->_cond.wait(locker, [this]() {return !this->_queRcvMessages.empty(); });// wait until the other thread tells me to start working
			}
			curr = this->_queRcvMessages.front();
			this->_queRcvMessages.pop();
			locker.unlock();
			auto it = this->_connectedUsers.find(curr->getSock());
			if (it != this->_connectedUsers.end())
				curr->setUser(it->second);
			else
				curr->setUser(nullptr);

			int msgCode = curr->getMessageCode();
			if (msgCode == Protocol::ClientServer::signUpGE)
			{
				this->handleSignUp(curr);
			}
			else if (msgCode == Protocol::ClientServer::signInGE)
			{
				this->handleSignin(curr);
			}
			else if (msgCode == Protocol::ClientServer::signInOutUp::signOut)
			{
				this->handleSignout(curr);
			}
			else if (msgCode == Protocol::ClientServer::createRoomGE)
			{
				this->handleCreateRoom(curr);
			}
			else if (msgCode == Protocol::ClientServer::joinARoomGE)
			{
				this->handleJoinRoom(curr);
			}
			else if (msgCode == Protocol::ClientServer::getRoomList)
			{
				this->handleGetRooms(curr);
			}
			else if (msgCode == Protocol::ClientServer::leaveRoom)
			{
				this->handleLeaveRoom(curr);
			}
			else if (msgCode == Protocol::ClientServer::leaveGame)
			{
				this->handleLeaveGame(curr);
			}
			else if (msgCode == Protocol::ClientServer::closeRoom)
			{
				this->handleCloseRoom(curr);
			}
			else if (msgCode == Protocol::ClientServer::getUsersListInRoomGE)
			{
				this->handleGetUsersInRoom(curr);
			}
			else if (msgCode == Protocol::ClientServer::game::startGame)
			{
				this->handleStartGame(curr);
			}
			else if (msgCode == Protocol::ClientServer::usersAnswerToQuestionGE)
			{
				vector<std::string> val = curr->getValues();
				User* user = curr->getUser();
				if (!user->getGame()->handleAnswerFromUser(user, atoi(val[AnsNoIndexInRcvMsgVal].c_str()), atoi(val[ansTimeIndexInRcvMsgVal].c_str())))
				{
					delete user->getGame();
				}
			}
			else if (msgCode == Protocol::ClientServer::getBestScores)
			{
				this->handleGetBestScores(curr);
			}
			else if (msgCode == Protocol::ClientServer::self::getSelfStatus)
			{
				this->handleGetPersonalStatus(curr);
			}
			else if (msgCode == Protocol::ClientServer::game::leaveGame)
			{
				this->handleLeaveGame(curr);
			}
			else if (msgCode == Protocol::ClientServer::leaveApp)
			{
				this->safeDeleteUser(curr);
			}
			else//if code unknown
			{
				this->safeDeleteUser(curr);
			}
		}

		catch (exception& e)
		{
			cout << "exception in TriviaServer::handleRecivedMessages: " << e.what() << endl;
			if (curr)
				delete curr;
			this->safeDeleteUser(curr);
		}
		if (curr)
			delete curr;

	}
}

void TriviaServer::addRecievedMessage(RecievedMessage * rcvMsg)
{
	std::lock_guard<std::mutex> m(this->_mtxRecieveMessages);
	this->_queRcvMessages.push(rcvMsg);
	this->_cond.notify_all();
}

RecievedMessage * TriviaServer::buildRecievedMessage(SOCKET s, int n)
{
	std::vector<std::string> val;
	RecievedMessage * recvMsg = nullptr;
	if (n == Protocol::ClientServer::signInOutUp::signInGE)
	{
		int userLength = Helper::getIntPartFromSocket(s, 2);
		std::string username = Helper::getStringPartFromSocket(s, userLength);
		int passLength = Helper::getIntPartFromSocket(s, 2);
		std::string password = Helper::getStringPartFromSocket(s, passLength);
		val.push_back(username);
		val.push_back(password);
	}
	else if (n == Protocol::ClientServer::signInOutUp::signUpGE)
	{
		int userLength = Helper::getIntPartFromSocket(s, 2);
		std::string username = Helper::getStringPartFromSocket(s, userLength);
		int passLength = Helper::getIntPartFromSocket(s, 2);
		std::string password = Helper::getStringPartFromSocket(s, passLength);
		int emailLength = Helper::getIntPartFromSocket(s, 2);
		std::string email = Helper::getStringPartFromSocket(s, emailLength);
		val.push_back(username);
		val.push_back(password);
		val.push_back(email);
	}
	else if (n == Protocol::ClientServer::createRoomGE)
	{
		int nameLength = Helper::getIntPartFromSocket(s, 2);
		std::string name = Helper::getStringPartFromSocket(s, nameLength);
		int numOfPlayers = Helper::getIntPartFromSocket(s, 1);
		int numOfQues = Helper::getIntPartFromSocket(s, 2);
		int ansTime = Helper::getIntPartFromSocket(s, 2);
		val.push_back(name);
		val.push_back(std::to_string(numOfPlayers));
		val.push_back(std::to_string(numOfQues));
		val.push_back(std::to_string(ansTime));
	}
	else if (n == Protocol::ClientServer::getUsersListInRoomGE)
	{
		int roomID = Helper::getIntPartFromSocket(s, 4);
		val.push_back(std::to_string(roomID));
	}
	else if (n == Protocol::ClientServer::joinARoomGE)
	{
		int roomID = Helper::getIntPartFromSocket(s, 4);
		val.push_back(std::to_string(roomID));
	}
	else if (n == Protocol::ClientServer::usersAnswerToQuestionGE)
	{
		char ansNum = Helper::getIntPartFromSocket(s, 1);
		int ansTime = Helper::getIntPartFromSocket(s, 2);
		val.push_back(std::to_string(ansNum));
		val.push_back(std::to_string(ansTime));
	}

	if (val.size() > 0)
		recvMsg = new RecievedMessage(s, n, val);
	else
		recvMsg = new RecievedMessage(s, n);
	//recvMsg->setUser(temp);
	return recvMsg;
}

User* TriviaServer::getUserByName(string name)
{
	auto it = this->_connectedUsers.begin();
	while (it != this->_connectedUsers.end())
	{
		if (it->second->getUsername() == name)
		{
			return it->second;
		}
		it++;
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET s)
{
	auto it = this->_connectedUsers.find(s);
	if (it != this->_connectedUsers.end())
		return it->second;
	return nullptr;
}

Room* TriviaServer::getRoomById(int id)
{
	auto it = this->_roomsList.find(id);
	if (it != this->_roomsList.end())
		return it->second;
	return nullptr;
}

//create table t_players_answers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null, primary key(game_id, username, question_id), foreign key(question_id) references t_questions(question_id), foreign key(game_id) references t_games(game_id), foreign key(username) references t_users(username));