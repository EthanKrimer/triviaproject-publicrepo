#include "DataBase.h"

int DataBase::callbackScores(void* notUsed, int argc, char** argv, char** azCol)
{
	auto it = scores.find(argv[1]);
	if (it != scores.end())
	{
		it->second += atoi(argv[4]);
	}
	else
	{
		pair<string, int> p;
		p.first = argv[1];
		p.second = atoi(argv[4]);
		scores.insert(p);
	}
	return 0;
}

void DataBase::clearScores()
{
	scores.clear();
}

int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	return 0;
}

void DataBase::clearResults()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

int DataBase::callbackQuestions(void* notUsed, int argc, char** argv, char** azCol)
{
	Question* que = new Question(atoi(argv[0]), string(argv[1]), string(argv[2]), string(argv[3]), string(argv[4]), string(argv[5]));
	questions.push_back(que);
	return 0;
}

void DataBase::clearQuestions()
{
	questions.clear();
}

int DataBase::callbackRawPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	auto it = RawPersonalStatus.find("game_id");
	if (it != RawPersonalStatus.end())
	{
		it->second.push_back(atoi(argv[0]));
	}
	else
	{
		pair<string, vector<int>> p;
		p.first = "game_id";
		p.second.push_back(atoi(argv[0]));
		RawPersonalStatus.insert(p);
	}

	it = RawPersonalStatus.find("is_correct");
	if (it != RawPersonalStatus.end())
	{
		it->second.push_back(atoi(argv[4]));
	}
	else
	{
		pair<string, vector<int>> p;
		p.first = "is_correct";
		p.second.push_back(atoi(argv[4]));
		RawPersonalStatus.insert(p);
	}

	it = RawPersonalStatus.find("answer_time");
	if (it != RawPersonalStatus.end())
	{
		it->second.push_back(atoi(argv[5]));
	}
	else
	{
		pair<string, vector<int>> p;
		p.first = "answer_time";
		p.second.push_back(atoi(argv[5]));
		RawPersonalStatus.insert(p);
	}

	return 0;
}

void DataBase::clearRawPersonalStatuse()
{
	for (auto it = RawPersonalStatus.begin(); it != RawPersonalStatus.end(); ++it)
	{
		it->second.clear();
	}
	RawPersonalStatus.clear();
}

DataBase::DataBase()
{
	this->rc = sqlite3_open(DB_NAME, &this->db);

	if (this->rc)
	{
		throw std::exception("cant open DB");
		sqlite3_close(this->db);
	}
}

DataBase::~DataBase()
{
	try
	{
		sqlite3_close(this->db);
	}
	catch (const std::exception& e) { cout << e.what() << endl; }
}

bool DataBase::isUserExists(string username)
{
	string DML = "select * from t_users where username = '" + username + "';";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
	}
	else if (results.size() != 0)
	{
		clearResults();
		return true;
	}
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	if (Validator::isUsernameValid(username) && Validator::isPasswordValid(password) && !this->isUserExists(username))
	{
		string DML = "INSERT INTO t_users (username, password, email) values('" + username + "', '" + password + "', '" + email + "');";
		this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
		if (this->rc != SQLITE_OK)
		{
			cout << "SQL error: " << this->zErrMsg << endl;
			sqlite3_free(this->zErrMsg);
			return false;
		}
		this->addAnswerToPlayer(-1, username, -1, "", false, -1);
		return this->isUserExists(username);
	}
	return false;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	if (Validator::isUsernameValid(username) && Validator::isPasswordValid(password))
	{
		string DML = "select * from t_users where username = '" + username + "' and password = '" + password + "';";
		this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
		if (this->rc != SQLITE_OK)
		{
			cout << "SQL error: " << this->zErrMsg << endl;
			sqlite3_free(this->zErrMsg);
			return false;
		}
		else if (results.size() != 0)
		{
			clearResults();
			return true;
		}
	}
	return false;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	vector<Question*> randomQuestions;
	string DML = "select * from t_questions;";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callbackQuestions, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return randomQuestions;
	}
	int* boolArr = new int[questions.size()]{};
	srand(time(NULL));
	int temp = rand() % questions.size();
	if (questions.size() < questionsNo)
		questionsNo = (int)questions.size();
	for (int i = 0; i < questionsNo; i++)
	{
		while (boolArr[temp] == 1)
			temp = rand() % questions.size();
		boolArr[temp] = 1;
		randomQuestions.push_back(questions[temp]);
	}
	delete boolArr;
	clearQuestions();
	return randomQuestions;
}

vector<pair<string, int>>  DataBase::getBestScores()
{
	//vector<string> bestScores;
	vector<pair<string, int>> sortedScores;
	string DML = "select * from t_players_answers;";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callbackScores, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return sortedScores;
	}

	for (auto iterator = scores.begin(); iterator != scores.end(); ++iterator)
		sortedScores.push_back(*iterator);

	std::sort(sortedScores.begin(), sortedScores.end(), sortbysec);

	//for (auto it = sortedScores.begin(); it != sortedScores.end(); ++it)
	//	bestScores.insert(pair<std::string, int>(it->first,it->second));

	//bestScores.push_back(it->first + ": " + to_string(it->second));

	clearScores();
	return sortedScores;
}

bool DataBase::sortbysec(const pair<string, int> &a, const pair<string, int> &b)
{
	return (a.second > b.second);
}

vector<string> DataBase::getPersonalStatus(string username)
{
	vector<string> personalStatus;
	string DML = "select * from t_players_answers where username = '" + username + "';";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callbackRawPersonalStatus, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return personalStatus;
	}
	unordered_set<int> uniqueGameIds;
	try
	{
		uniqueGameIds.insert(RawPersonalStatus.find("game_id")->second.begin(), RawPersonalStatus.find("game_id")->second.end());
		int noGames = 0;
		for (int x: uniqueGameIds)
		{
			if (x == -1)
			{
				noGames++;
			}
		}
		personalStatus.push_back(to_string(uniqueGameIds.size() - noGames));
		pair<int, int> score;
		for (const int& ans : RawPersonalStatus.find("is_correct")->second)
		{
			if (ans)
				score.first++;
			else
				score.second++;
		}
		personalStatus.push_back(to_string(score.first));
		personalStatus.push_back(to_string(score.second));
		double totalTime = 0;
		int noTime = 0;
		for (const int& ansTime : RawPersonalStatus.find("answer_time")->second)
		{
			if (ansTime != -1)
			{
				totalTime += ansTime;
			}
			else
			{
				noTime++;
			}
		}
		personalStatus.push_back(to_string(totalTime / RawPersonalStatus.find("answer_time")->second.size() - noTime));
		clearRawPersonalStatuse();
		return personalStatus;
	}
	catch (...)
	{
		personalStatus.resize(0);
		return personalStatus;
	}
	
}

int DataBase::insertNewGame()
{
	string DML = "insert into t_games(status, start_time) values(0, CURRENT_TIMESTAMP);";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return -1;
	}
	else
	{
		DML = "select game_id from t_games;";
		this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
		if (this->rc != SQLITE_OK)
		{
			cout << "SQL error: " << this->zErrMsg << endl;
			sqlite3_free(this->zErrMsg);
			return -1;
		}
		else
		{
			int gameID = stoi(results.find("game_id")->second.back());
			clearResults();
			return gameID;
		}
	}
}

bool DataBase::updateGameStatus(int gameID)
{
	string DML = "update t_games set status = 1, end_time = CURRENT_TIMESTAMP where game_id = " + to_string(gameID) + ";";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	string DML = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" + to_string(gameId) + ", '" + username + "', " + to_string(questionId) + ", '" + answer + "', " + to_string(isCorrect) + ", " + to_string(answerTime) + ");";
	this->rc = sqlite3_exec(this->db, DML.c_str(), callback, 0, &this->zErrMsg);
	if (this->rc != SQLITE_OK)
	{
		cout << "SQL error: " << this->zErrMsg << endl;
		sqlite3_free(this->zErrMsg);
		return false;
	}
	return true;
}