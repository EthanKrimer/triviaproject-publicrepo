#pragma once
#include <string>
class Validator
{
public:
	static bool isPasswordValid(std::string pass);
	static bool isUsernameValid(std::string username);

};