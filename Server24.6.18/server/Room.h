#pragma once
#include <vector>
#include <string>
#include "User.h"
#include "Protocol.h"

class User;

class Room
{
private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	std::string _name;
	int _id;

	std::string getUsersAsString(std::vector<User*> vec,User* user);
	void sendMessage(std::string msg);
	void sendMessage(User * excludeUser, std::string msg);

public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionTime, int questionNo);
	~Room();
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	std::vector<User*> getUsers();
	std::string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	std::string getName();
	User * getAdmin();

};