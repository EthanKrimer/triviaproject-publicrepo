#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include <iostream>
#include "TriviaServer.h"
#include <exception>

int main(void)
{
	try
	{
		WSAInitializer wsaInit;
		TriviaServer * t = new TriviaServer();
		t->server();
	}
	catch (std::exception& e)
	{
		cout << "Error occured: " << e.what() << endl;
	}

	system("PAUSE");
	return 0;
}