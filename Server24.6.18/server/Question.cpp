#include "Question.h"
#include <iostream>
Question::Question(int id, std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4)
{
	this->_id = id;
	this->_question = question;
	std::string* temp = new std::string[4];
	temp[0] = correctAnswer; temp[1] = answer2; temp[2] = answer3; temp[3] = answer4;
	srand(time(NULL));
	int tempIndex = rand() % 4;
	for (int i = 0; i < 4; i++)
	{
		tempIndex = rand() % 4;
		while (temp[tempIndex] == "")
			tempIndex = (tempIndex + 1) % 4;
		this->_answers[i] = temp[tempIndex];
		if (this->_answers[i] == correctAnswer)
			this->_correctAnswerIndex = i;
		temp[tempIndex] = "";
	}
	this->_ansArrLength = sizeof(this->_answers) / sizeof(this->_answers[0]);
	/*
	for (int i = 0; i < _ansArrLength; i++)
	{
		std::cout << this->_answers[i] <<std::endl;
	}*/
	delete[] temp;
}

Question::~Question()
{
}

std::string Question::getQuestion()
{
	return this->_question;
}

std::string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getArrLength()
{
	return this->_ansArrLength;
}

int Question::getID()
{
	return this->_id;
}
