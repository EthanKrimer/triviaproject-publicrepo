#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int code)
{
	this->_sock = sock;
	this->_messageCode = code;
	this->_user = nullptr;
}

RecievedMessage::RecievedMessage(SOCKET sock, int code, std::vector<std::string> values)
{
	this->_sock = sock;
	this->_messageCode = code;
	this->_values = values;
	this->_user = nullptr;
}

SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User * user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

std::vector<std::string>& RecievedMessage::getValues()
{
	return this->_values;
}
