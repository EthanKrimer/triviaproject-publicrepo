#include "Game.h"

/*
function inserts game to DB using DB::insertNewGame
sets the game _id
returns false if the _id is -1
returns true if got an actual _id
*/
bool Game::insertGameToDB()
{
	this->_id = this->_db.insertNewGame();
	if (this->_id == -1)
	{
		return false;
	}
	return true;
}

/*
function initiates questions from db using DB::initQuestions
sets the game _questions vector to the returned vector from the DB class
*/
void Game::initQuestionsFromDB()
{
	this->_questions = this->_db.initQuestions(this->_questions_no);
}

/*
function sends the question and answers to all users
protocol:118
*/
void Game::sendQuestionToAllUsers()
{
	//118SIZEOFQUESTIONquestionSIZEOFANSans
	//1180 if fail- to admin only---if questionLength 0
	Question * question = this->_questions[this->_currQuestionIndex];
	std::string ques = question->getQuestion();
	if (ques.length() > 0)
	{
		std::string * answers = question->getAnswers();
		this->_currentTurnAnswers = 0;
		std::string ret = std::to_string(Protocol::ServerClient::QuestionsAndAnswers::sendQuestionGE)
			+ Helper::getPaddedNumber(ques.length(), 3)
			+ ques;
		for (int i = 0; i < question->getArrLength(); i++)
		{
			ret += Helper::getPaddedNumber(answers[i].length(), 3);
			ret += answers[i];
		}
		for (unsigned int i = 0; i < this->_players.size(); i++)
		{
			try
			{
				this->_players[i]->send(ret);
			}
			catch (std::exception & e)
			{
				cout << "exception in Game::sendQuestionsToAllUsers, while sending to users: " << e.what() << endl;
			}
		}
	}
	else
	{
		std::string ret = std::to_string(Protocol::ServerClient::QuestionsAndAnswers::sendQuesFail);
		this->_players[0]->getRoom()->getAdmin()->send(ret);
	}
}

/*
cto'r
*/
Game::Game(const std::vector<User*>& players, int questionNo, DataBase & DB) : _db(DB)
{
	this->_players = players;
	this->_questions_no = questionNo;
	if (!this->insertGameToDB())
		throw std::exception("error in Game::Game() can't insert new game to DB, got -1 on game id");
	else
	{
		this->initQuestionsFromDB();
		for (unsigned int i = 0; i < this->_players.size(); i++)
		{
			User * curr = this->_players[i];
			curr->setGame(this);
			this->_results.insert(std::pair<std::string, int>(curr->getUsername(), 0));
		}
		this->_status = 0;//game is active
	}
}

/*
dto'r
*/
Game::~Game()
{
	//for now lets just empty the vectors
	this->_players.clear();
	this->_questions.clear();
}

/*
just sends the first question
*/
void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

/*
handle for finishing game
sends each user the cscore, sets _status to 1, calls the DB class to set the status on the actual db to 1
protocol:121
*/
void Game::handleFinishGame()
{
	if (!this->_db.updateGameStatus(this->_id))
		throw std::exception("Exception in Game::handleFinishGame, while using DB::updateGameStatus, recieved false from the db function.");
	this->_status = 1;
	string msg = std::to_string(Protocol::ServerClient::gameFinished::gameFinishedGE) + Helper::getPaddedNumber(this->_players.size(), 1);
	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			User * curr = this->_players[i];
			std::string user = curr->getUsername();
			msg += Helper::getPaddedNumber(user.length(), 2);
			msg = msg + user + Helper::getPaddedNumber(_results[user], 2);
			//curr->clearGame();
		}
		catch (std::exception & e)
		{
			cout << "Exception in Game::handleFinishGame, while creating msg: " << e.what() << endl;
		}
	}
	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			User * curr = this->_players[i];
			curr->send(msg);
			//curr->clearGame();
		}
		catch (std::exception & e)
		{
			cout << "Exception in Game::handleFinishGame, while sending msg to users: " << e.what() << endl;
		}
	}
}

/*
function handles next turn
checks if there are players in game, if there are players in the room and if the status is 0(is the game not finished)
checks if everyone answered
checks if it was the last question
calls handle finish game
if not, we continue to next question, goes to send question to all users func
if not, returns true(game still running)
if not, finishes game, returns false.
*/
bool Game::handleNextTurn()
{
	bool ret = false;
	if (this->_players.size() != 0 && this->_status == 0)//is game still active and there 
	{																											 //are players in the room
		if (this->_currentTurnAnswers == this->_players.size())
		{
			if (this->_questions[this->_currQuestionIndex] == this->_questions[this->_questions.size() - 1])
			{
				this->handleFinishGame();
			}
			else
			{
				ret = true;
				this->_currQuestionIndex++;
				this->sendQuestionToAllUsers();
			}
		}
		else//not everyone answered
		{
			ret = true;
		}
	}
	else
	{
		this->handleFinishGame();
	}
	return ret;
}

/*
function handles the answer,
checks if it is correct, takes care of his score, sends him a about the answer being correct or not.
calls handle next turn(returns the value from that func)
*/
bool Game::handleAnswerFromUser(User * user, int answerNo, int time)
{
	int correct = 0;
	if (this->_status == 0)// game running
	{
		std::string userAns;
		Question * que = this->_questions[this->_currQuestionIndex];
		this->_currentTurnAnswers++;
		bool isCorrect = false;
		if (answerNo == 5)
		{
			userAns = "";
		}
		else
		{
			std::string correctAns = que->getAnswers()[que->getCorrectAnswerIndex()];
			userAns = que->getAnswers()[answerNo - 1];
			if (userAns == correctAns)
			{
				this->_results[user->getUsername()] ++;
				isCorrect = true;
				correct = 1;
			}
		}
		this->_db.addAnswerToPlayer(this->_id, user->getUsername()//bool
			, que->getID(), userAns, isCorrect, time);
	}
	user->send(std::to_string(Protocol::ServerClient::QuestionsAndAnswers::isAnsCorrectGE) + Helper::getPaddedNumber(correct, 1));
	return this->handleNextTurn();
}

/*
removes user from game, returns true if game still running(are there players left?)
false if not.
*/
bool Game::leaveGame(User * currUser)
{
	auto it = this->_players.begin();
	while (it != this->_players.end())
	{
		if (*it == currUser)
		{
			this->_players.erase(it);
			break;
		}
		it++;
	}
	if (this->_players.size() == 0)
	{
		return false;
	}
	return true;
}

int Game::getID()
{
	return this->_id;
}