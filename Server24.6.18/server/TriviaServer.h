#pragma once
#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <string>
#include <utility>
#include <exception>
#include <fstream>
#include <WinSock2.h>
#include <map>
#include "User.h"
#include "DataBase.h"
#include "Room.h"
#include "RecievedMessage.h"
#include "Helper.h"
#include "Protocol.h"
#define LISTENING_PORT 8643

using std::queue;
using std::string;
using std::pair;
using std::cout;
using std::endl;
using std::thread;
using std::exception;
using std::map;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void server();

private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	std::map<int, Room*> _roomsList;
	std::mutex _mtxRecieveMessages;
	std::queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;
	std::condition_variable _cond;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET clientSocket);
	void safeDeleteUser(RecievedMessage* rcvMsg);

	User* handleSignin(RecievedMessage* rcvMsg);
	bool handleSignUp(RecievedMessage* rcvMsg);
	void handleSignout(RecievedMessage* rcvMsg);

	void handleLeaveGame(RecievedMessage* rcvMsg);
	void handleStartGame(RecievedMessage* rcvMsg);
	void handlePlayerAnswer(RecievedMessage* rcvMsg);

	bool handleCreateRoom(RecievedMessage* rcvMsg);
	bool handleCloseRoom(RecievedMessage* rcvMsg);
	bool handleJoinRoom(RecievedMessage* rcvMsg);
	bool handleLeaveRoom(RecievedMessage* rcvMsg);
	void handleGetUsersInRoom(RecievedMessage* rcvMsg);
	void handleGetRooms(RecievedMessage* rcvMsg);

	void handleGetBestScores(RecievedMessage* rcvMsg);
	void handleGetPersonalStatus(RecievedMessage* rcvMsg);

	void handleRecivedMessages();
	void addRecievedMessage(RecievedMessage* rcvMsg);
	RecievedMessage* buildRecievedMessage(SOCKET s, int n);

	User* getUserByName(string name);
	User* getUserBySocket(SOCKET s);
	Room* getRoomById(int id);

};