#include "Room.h"

/*
Function returns a string including all the users int the vector except for the excluded user.
*/
std::string Room::getUsersAsString(std::vector<User*> vec, User * excludeUser)
{
	std::string ret = "";
	for (unsigned int i = 0; i < vec.size(); i++)
	{
		if (vec[i] != excludeUser)
		{
			ret += vec[i]->getUsername();
		}
	}
	return ret;
}

/*
Sends a msg to all the users
*/
void Room::sendMessage(std::string msg)
{
	this->sendMessage(nullptr, msg);
}

/*
Sends a msg to all the users except the excluded one
*/
void Room::sendMessage(User * excludeUser, std::string msg)
{
	try
	{
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i] != excludeUser)
			{
				this->_users[i]->send(msg);
			}
		}
	}
	catch (std::exception & e)
	{
		cout << "exception in Room::sendMessage: " << e.what() << endl;
	}
}

/*
cto'r
*/
Room::Room(int id, User * admin, std::string name, int maxUsers, int questionTime, int questionNo)
{
	_id = id;
	_admin = admin;
	_users.push_back(admin);
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionTime = questionTime;
}

Room::~Room()
{
	this->_users.clear();
}

/*
Adds the user to the room if its able to
*/
bool Room::joinRoom(User * user)
{
	if (this->_users.size() == this->_maxUsers)
	{
		user->send(std::to_string(Protocol::ServerClient::joiningAnExistingRoom::failedRoomIsFull));
		return false;
	}
	else if (this->_users.size() > 0)
	{
		this->_users.push_back(user);
		user->send(std::to_string(Protocol::ServerClient::joiningAnExistingRoom::joiningAnExistingRoomGE) +
			Helper::getPaddedNumber(this->_questionNo, 2) +
			Helper::getPaddedNumber(this->_questionTime, 2));
		this->sendMessage(this->getUsersListMessage());
		return true;
	}
	else
	{
		user->send(std::to_string(Protocol::ServerClient::joiningAnExistingRoom::failedRoomDoesntExistOrOtherReason));
		return false;
	}
}

/*
removes user from room, sends a msg to all the users updating about the current users list.
*/
void Room::leaveRoom(User * user)
{
	auto it = this->_users.begin();
	while (it != this->_users.end())
	{
		if (*it == user)
		{
			this->_users.erase(it);
			user->send(std::to_string(Protocol::ServerClient::room::leaveARoom));
			this->sendMessage(this->getUsersListMessage());
			break;
		}
		it++;
	}
}

/*
closes room, sends everyone msg about room closing, uses User::clearRoom() to set each _room of the users to nullptr.
*/
int Room::closeRoom(User * user)
{
	if (this->_admin == user)
	{
		this->sendMessage(std::to_string(Protocol::ServerClient::room::closeRoom));
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i] != user)
			{
				this->_users[i]->clearRoom();
			}
		}
		return this->getId();
	}
	else
	{
		return -1;
	}
}

std::vector<User*> Room::getUsers()
{
	return this->_users;
}

/*
gets the user list msg, according to protocol.
*/
std::string Room::getUsersListMessage()
{

	std::string ret = std::to_string(Protocol::ServerClient::room::sendUsersListRoomGE) + std::to_string(this->_users.size());
	std::string temp;
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		temp = (_users[i])->getUsername();
		ret += Helper::getPaddedNumber(temp.length(), 2);
		ret += temp;
	}
	return ret;
}

int Room::getQuestionsNo()
{
	return this->_questionNo;
}

int Room::getId()
{
	return this->_id;
}

std::string Room::getName()
{
	return this->_name;
}

User * Room::getAdmin()
{
	return this->_admin;
}
