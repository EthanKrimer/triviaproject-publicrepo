#pragma once

#include <algorithm>
#include <iostream>
#include "sqlite3.h"
#include <vector>
#include <string>
#include "Question.h"
#include <exception>
#include <unordered_map>
#include <utility>
#include "Validator.h"
#include <time.h>
#include <unordered_set>

#define DB_NAME "trivia.db"

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::unordered_map;
using std::unordered_set;
using std::pair;
using std::to_string;

static unordered_map<string, vector<string>> results;
static vector<Question*> questions;
static unordered_map<string, int> scores;
static unordered_map<string, vector<int>> RawPersonalStatus;

class DataBase
{
private:
	int rc;
	sqlite3* db;
	char* zErrMsg = 0;
	bool flag = true;

	/*static int callbackCount(void*, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);*/

	static bool sortbysec(const pair<string, int> &a, const pair<string, int> &b);//helper func for getBestScores

	static int callbackScores(void*, int, char**, char**);
	static void clearScores();
	static int callback(void*, int, char**, char**);
	static void clearResults();
	static int callbackQuestions(void*, int, char**, char**);
	static void clearQuestions();
	static int callbackRawPersonalStatus(void*, int, char**, char**);
	static void clearRawPersonalStatuse();

public:
	DataBase();
	~DataBase();
	bool isUserExists(std::string);
	bool addNewUser(std::string, std::string, std::string);
	bool isUserAndPassMatch(std::string, std::string);
	std::vector<Question*> initQuestions(int);
	vector<pair<string, int>> getBestScores();
	std::vector<std::string> getPersonalStatus(std::string); //returns vector of string: num of games, num of right ans, num of wrong ans, avg time for ans
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, std::string, int, std::string, bool, int);

};