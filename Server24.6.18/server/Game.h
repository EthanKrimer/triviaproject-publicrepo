#pragma once

#include "Question.h"
#include "User.h"
#include "DataBase.h"
#include <vector>
#include <map>
class User;
//class DataBase;

class Game
{
private:
	std::vector<Question*> _questions;
	std::vector<User*> _players;
	int _questions_no;
	int _id;
	int _currQuestionIndex;
	DataBase & _db;
	std::map<std::string, int> _results;
	int _currentTurnAnswers;
	int _status;

	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

public:
	Game(const std::vector<User*>& players, int questionNo, DataBase& DB);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
	int getID();

};
