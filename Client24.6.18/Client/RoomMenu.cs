﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;


namespace WindowsFormsApp1
{
    public partial class RoomMenu : Form
    {
        private List<string> _roomsNames = new List<string>();
        private List<string> _roomsIds = new List<string>();
        private TriviaMenu _triviaMenu;
        private NetworkStream _clientStream;

        /*gets data and sets it to the list box*/
        private void getDataFromListAndSetIt(List<string>list)
        {
            this._roomsIds.Clear();
            this._roomsNames.Clear();
            for (int i = 0; i < list.Count ; i = i + 2)
            {
                this._roomsIds.Add(list[i]);
            }
            for (int i = 1; i < list.Count; i = i + 2)
            {
                this._roomsNames.Add(list[i]);
            }
            BindingList<string> bindingList = new BindingList<string>(this._roomsNames);
            this.roomList.DataSource = bindingList;
            this.roomList.Refresh();
        }

        public RoomMenu(List<string> list, TriviaMenu trivia, NetworkStream client)
        {
            InitializeComponent();
            _clientStream = client;
            this.usersList.Hide();
            this.ErrorMsg.Hide();
            _triviaMenu = trivia;
            this.getDataFromListAndSetIt(list);//gets data and sets it to the list box
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            byte[] bufferIn;
            int bytesRead;
            this.usersList.Hide();
            this.ErrorMsg.Hide();
            string roomId = this._roomsIds[this.roomList.SelectedIndex];
            string msgToSend = ((int)msgControl.ClientServer.room.getUsersListInRoomGE).ToString() + TriviaMenu.intToByteToString(Int32.Parse(roomId),4);
            byte[] buffer = new ASCIIEncoding().GetBytes(msgToSend);
            this._clientStream.Write(buffer, 0, buffer.Length);
            this._clientStream.Flush();
            string rcvM = "";
            while(rcvM == "" || rcvM != ((int)msgControl.ServerClient.room.sendUsersListRoomGE).ToString())
            {
                try
                {
                    bufferIn = new byte[3];
                    bytesRead = this._clientStream.Read(bufferIn, 0, 3);
                    rcvM = new ASCIIEncoding().GetString(bufferIn);
                }
                catch
                {
                    //didnt get anything, continue.
                }
            }
            bufferIn = new byte[4096];
            bytesRead = this._clientStream.Read(bufferIn, 0, 4096);
            string parseThis = new ASCIIEncoding().GetString(bufferIn);
            int numOfUsers = Int32.Parse(parseThis.Substring(0, 1));
            parseThis = parseThis.Remove(0, 1);
            List<string> list = new List<string>();
            for(int i = 0; i < numOfUsers; i++)
            {
                int userLen = Int32.Parse(parseThis.Substring(0, 2).TrimStart('0'));
                parseThis = parseThis.Remove(0, 2);
                list.Add(parseThis.Substring(0, userLen));
                parseThis = parseThis.Remove(0, userLen);
            }
            this.usersList.DataSource = list;
            this.usersList.Show();
        }

        private void usersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //nothing
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            this.usersList.Hide();
            this.ErrorMsg.Hide();
            string msgToSend = ((int)msgControl.ClientServer.room.getRoomList).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(msgToSend);
            this._clientStream.Write(buffer, 0, buffer.Length);
            this._clientStream.Flush();
            string rcvM = "";
            byte[] bufferIn;
            int bytesRead;
            while (rcvM == "" || rcvM != ((int)msgControl.ServerClient.room.sendRoomsListGE).ToString())
            {
                try
                {
                    bufferIn = new byte[3];
                    bytesRead = this._clientStream.Read(bufferIn, 0, 3);
                    rcvM = new ASCIIEncoding().GetString(bufferIn);
                }
                catch
                {
                    //didnt get anything, continue.
                }
            }
            bufferIn = new byte[4096];
            bytesRead = this._clientStream.Read(bufferIn, 0, 4096);
            string parseThis = new ASCIIEncoding().GetString(bufferIn);
            List<string> list = this._triviaMenu.GetRoomsList(parseThis);
            this.getDataFromListAndSetIt(list);//gets data and sets it to the list box
        }

        private void joinButton_Click(object sender, EventArgs e)
        {
            if (this.roomList.SelectedItem == null)
            {
                return;
            }
            string roomId = this._roomsIds[this.roomList.SelectedIndex];
            string msgToSend = ((int)msgControl.ClientServer.room.joinARoomGE).ToString() + 
                TriviaMenu.intToByteToString(Int32.Parse(roomId), 4);
            byte[] buffer = new ASCIIEncoding().GetBytes(msgToSend);
            this._clientStream.Write(buffer, 0, buffer.Length);
            this._clientStream.Flush();
            string rcvM = "";
            byte[] bufferIn;
            int bytesRead;
            while (rcvM == "" || rcvM.Substring(0,3) != ((int)msgControl.ServerClient.joiningAnExistingRoom.joiningAnExistingRoomGE).ToString())
            {
                try
                {
                    bufferIn = new byte[4096];
                    bytesRead = this._clientStream.Read(bufferIn, 0, 4096);
                    rcvM = new ASCIIEncoding().GetString(bufferIn);
                }
                catch
                {
                    //didnt get anything, continue.
                }
            }
            if (rcvM.Substring(0, 4) == ((int)msgControl.ServerClient.joiningAnExistingRoom.successJoiningRoom).ToString())
            {
                //success
                this.Hide();
                waitingForPlayers wind = new waitingForPlayers(this._triviaMenu,this._roomsNames[this.roomList.SelectedIndex], this._roomsIds[this.roomList.SelectedIndex], rcvM);
                wind.ShowDialog();
                wind.Dispose();
                this.Close();
            }
            else if(rcvM.Substring(0, 4) == ((int)msgControl.ServerClient.joiningAnExistingRoom.failedRoomIsFull).ToString())
            {
                this.ErrorMsg.Text = "Failed.\nRoom is full.";
                this.ErrorMsg.Show();
            }
            else
            {
                this.ErrorMsg.Text = "I have no idea what just happened.\nsomething was wrong.";
                this.ErrorMsg.Show();
            }
        }
    }
}
