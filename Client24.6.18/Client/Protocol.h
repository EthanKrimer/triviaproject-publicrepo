#pragma once
class Protocol
{
	/*
	Notice!
	if you see "GE" in the end of an enum, it is because its a 
	general code, the rest of the msg needs to be added in the class that uses this enum
	*/
public:
	//server-client:
	struct ServerClient
	{
		enum SignInServerFeedback
		{
			Success = 1020,
			WrongDetails = 1021,
			UserIsAlreadyConnected = 1022
		};
		enum SignUpServerFeedback
		{
			SignUpSuccess = 1040,
			PassIllegal,
			UsernameAlreadyExist,
			UsernameIllegal,
			other
		};
		enum joiningAnExistingRoom
		{
			joiningAnExistingRoomGE = 1100,
			failedRoomIsFull = 1101,
			failedRoomDoesntExistOrOtherReason

		};
		enum room
		{
			createRoomSuccess = 1140,
			createRoomFail,
			sendRoomsListGE = 106,
			sendUsersListRoomGE = 108,
			failToSendListOfUsers = 1080,
			leaveARoom = 1120,
			closeRoom = 116

		};
		enum QuestionsAndAnswers
		{
			sendQuestionGE = 118,
			sendQuesFail = 1180,
			isAnsCorrectGE = 120
		};
		enum gameFinished
		{
			gameFinishedGE = 121
		};
		enum bestScores
		{
			bestScoresGE = 124
		};
		enum selfStatus
		{
			selfStatusGE = 126
		};
	}typedef ServerClient;

	struct ClientServer
	{
		enum signInOutUp
		{
			signInGE = 200,
			signOut,
			signUpGE = 203
		};
		enum room
		{
			getRoomList = 205,
			getUsersListInRoomGE = 207,
			joinARoomGE = 209,
			leaveRoom = 211,
			createRoomGE = 213,
			closeRoom = 215
		};
		enum game
		{
			startGame = 217,//sent by admin only
			usersAnswerToQuestionGE = 219,
			leaveGame = 222
		};
		enum self
		{
			getBestScores = 223,
			getSelfStatus = 225,
			leaveApp = 299
		};
	}typedef ClientServer;
	
	


};
