﻿namespace WindowsFormsApp1
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameLabel = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.numOfQuestionsLeftLabel = new System.Windows.Forms.Label();
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.ans1Button = new System.Windows.Forms.Button();
            this.ans3Button = new System.Windows.Forms.Button();
            this.ans4Button = new System.Windows.Forms.Button();
            this.ans2Button = new System.Windows.Forms.Button();
            this.timeLabel = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.scoreLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(12, 9);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(59, 13);
            this.usernameLabel.TabIndex = 0;
            this.usernameLabel.Text = "uesername";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Location = new System.Drawing.Point(309, 9);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(58, 13);
            this.roomNameLabel.TabIndex = 1;
            this.roomNameLabel.Text = "roomName";
            // 
            // numOfQuestionsLeftLabel
            // 
            this.numOfQuestionsLeftLabel.AutoSize = true;
            this.numOfQuestionsLeftLabel.Location = new System.Drawing.Point(13, 26);
            this.numOfQuestionsLeftLabel.Name = "numOfQuestionsLeftLabel";
            this.numOfQuestionsLeftLabel.Size = new System.Drawing.Size(103, 13);
            this.numOfQuestionsLeftLabel.TabIndex = 2;
            this.numOfQuestionsLeftLabel.Text = "numOfQuestionsLeft";
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.Location = new System.Drawing.Point(128, 50);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(30, 13);
            this.QuestionLabel.TabIndex = 3;
            this.QuestionLabel.Text = "ques";
            // 
            // ans1Button
            // 
            this.ans1Button.Location = new System.Drawing.Point(53, 66);
            this.ans1Button.Name = "ans1Button";
            this.ans1Button.Size = new System.Drawing.Size(272, 23);
            this.ans1Button.TabIndex = 4;
            this.ans1Button.Text = "button1";
            this.ans1Button.UseVisualStyleBackColor = true;
            this.ans1Button.Click += new System.EventHandler(this.ans1Button_Click);
            // 
            // ans3Button
            // 
            this.ans3Button.Location = new System.Drawing.Point(53, 124);
            this.ans3Button.Name = "ans3Button";
            this.ans3Button.Size = new System.Drawing.Size(272, 23);
            this.ans3Button.TabIndex = 5;
            this.ans3Button.Text = "button2";
            this.ans3Button.UseVisualStyleBackColor = true;
            this.ans3Button.Click += new System.EventHandler(this.ans3Button_Click);
            // 
            // ans4Button
            // 
            this.ans4Button.Location = new System.Drawing.Point(53, 153);
            this.ans4Button.Name = "ans4Button";
            this.ans4Button.Size = new System.Drawing.Size(272, 23);
            this.ans4Button.TabIndex = 6;
            this.ans4Button.Text = "button3";
            this.ans4Button.UseVisualStyleBackColor = true;
            this.ans4Button.Click += new System.EventHandler(this.ans4Button_Click);
            // 
            // ans2Button
            // 
            this.ans2Button.Location = new System.Drawing.Point(53, 95);
            this.ans2Button.Name = "ans2Button";
            this.ans2Button.Size = new System.Drawing.Size(272, 23);
            this.ans2Button.TabIndex = 7;
            this.ans2Button.Text = "button4";
            this.ans2Button.UseVisualStyleBackColor = true;
            this.ans2Button.Click += new System.EventHandler(this.ans2Button_Click);
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(12, 179);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(84, 13);
            this.timeLabel.TabIndex = 8;
            this.timeLabel.Text = "time left for ques";
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(321, 26);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 9;
            this.exitButton.Text = "exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // scoreLabel
            // 
            this.scoreLabel.AutoSize = true;
            this.scoreLabel.Location = new System.Drawing.Point(12, 203);
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(33, 13);
            this.scoreLabel.TabIndex = 10;
            this.scoreLabel.Text = "score";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 242);
            this.Controls.Add(this.scoreLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.ans2Button);
            this.Controls.Add(this.ans4Button);
            this.Controls.Add(this.ans3Button);
            this.Controls.Add(this.ans1Button);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.numOfQuestionsLeftLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.usernameLabel);
            this.Name = "Game";
            this.Text = "Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Label numOfQuestionsLeftLabel;
        private System.Windows.Forms.Label QuestionLabel;
        private System.Windows.Forms.Button ans1Button;
        private System.Windows.Forms.Button ans3Button;
        private System.Windows.Forms.Button ans4Button;
        private System.Windows.Forms.Button ans2Button;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label scoreLabel;
    }
}