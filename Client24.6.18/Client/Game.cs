﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace WindowsFormsApp1
{
    public partial class Game : Form
    {
        private int _timeForQues;
        private NetworkStream _stream;
        private int _numOfQuesTotal;
        private int _numOfQuesDone = 0;
        private int _numOfCorrectAns = 0;
        private int _currQuesNum = 1;
        private int _timerLeft;
        private Thread _t;
        static bool done = false;
        private System.Timers.Timer timer1;
        public Game(string username, string numOfQuestions, string roomName, string timeForQuestion, NetworkStream stream, string firstMsg)
        {
            InitializeComponent();
            this._timeForQues = Int32.Parse(timeForQuestion);
            this._stream = stream;
            this._numOfQuesTotal = Int32.Parse(numOfQuestions);
            this.usernameLabel.Text = username;
            this.numOfQuestionsLeftLabel.Text = "Questions: 1/" + numOfQuestions;
            this.roomNameLabel.Text = roomName;
            this.timeLabel.Text = this._timeForQues.ToString();
            this._stream = stream;
            this._timerLeft = this._timeForQues;
            this.scoreLabel.Text = "Score:0/0";
            timer1 = new System.Timers.Timer(1000);
            timer1.Enabled = true;
            timer1.Elapsed += new System.Timers.ElapsedEventHandler(timer_tick);
            this.setQuesAndAnswers(firstMsg);
            this._t = new Thread(gameLoop);
            this._t.Start();
            done = false;
        }
        private void setQuesAndAnswers(string msg)
        {
            if(msg.Substring(0,3) == ((int)msgControl.ServerClient.QuestionsAndAnswers.sendQuestionGE).ToString())
            {
                msg = msg.Remove(0, 3);
                List<string> list = new List<string>();
                for (int i = 0; i < 5; i++)
                {
                    int length = Int32.Parse(msg.Substring(0, 3).TrimStart('0'));
                    msg = msg.Remove(0, 3);
                    list.Add(msg.Substring(0, length));
                    msg = msg.Remove(0, length);
                }
                this.QuestionLabel.Text = list[0];
                this.ans1Button.Text = list[1];
                this.ans2Button.Text = list[2];
                this.ans3Button.Text = list[3];
                this.ans4Button.Text = list[4];
                this._timerLeft = this._timeForQues;
                this.timeLabel.Text = this._timeForQues.ToString();
                this.timer1.Start();
                this.ans1Button.Enabled = true;
                this.ans2Button.Enabled = true;
                this.ans3Button.Enabled = true;
                this.ans4Button.Enabled = true;
            }
        }

        private void gameLoop()
        {
            Thread.Sleep(150);
            while (this._numOfQuesDone <= this._numOfQuesTotal)
            { 
                byte[] bufferIn;
                int bytesRead;
                string rcvM;
                try
                {

                    bufferIn = new byte[4096];
                    bytesRead = this._stream.Read(bufferIn, 0, 4096);
                    rcvM = new ASCIIEncoding().GetString(bufferIn);
                    Invoke((MethodInvoker)
                    delegate
                    {
                        setQuesAndAnswers(rcvM);
                        isAnsCorrect(rcvM);
                        gotEndOfGameMsg(rcvM);
                    });
                }
                catch
                {
                    //didnt get anything, continue.
                }
            }
            if(!done)//done is true only if we clicked on the exit button!
            {
                Invoke((MethodInvoker)delegate
                { this.Close(); });
            }
            return;
        }

        private void gotEndOfGameMsg(string rcvMsg)
        {
            if(rcvMsg.Substring(0,3) == ((int)msgControl.ServerClient.gameFinished.gameFinishedGE).ToString())
            {
                rcvMsg = rcvMsg.Remove(0, 3);
                this._numOfQuesDone = this._numOfQuesTotal + 1;
                string msg = "Game is finished.\nScores:(Usename, score)\n";
                int numOfPlayers = Int32.Parse(rcvMsg.Substring(0, 1));
                rcvMsg = rcvMsg.Remove(0, 1);
                for (int i = 0; i < numOfPlayers; i++)
                {
                    int length = Int32.Parse(rcvMsg.Substring(0, 2).TrimStart('0'));
                    rcvMsg = rcvMsg.Remove(0, 2);
                    msg += rcvMsg.Substring(0, length);
                    rcvMsg = rcvMsg.Remove(0, length);
                    msg += ", ";
                    msg += rcvMsg.Substring(0, 2);
                    msg += '\n';
                    rcvMsg = rcvMsg.Remove(0, 2);
                }
                MessageBox.Show(msg);
            }
        }
        private void isAnsCorrect(string rcvMsg)
        {
            if(rcvMsg.Substring(0, 3) == ((int)msgControl.ServerClient.QuestionsAndAnswers.isAnsCorrectGE).ToString())
            {
                if(rcvMsg[3] == '1')//we gave a correct ans
                {
                    this._numOfCorrectAns++;
                }
                this.scoreLabel.Text = "Score: " + this._numOfCorrectAns.ToString()
                        + "/" + this._numOfQuesDone;
            }
        }

        private void sendAnsToServer(int index)//from 1 to 4. if its 5 player didnt answer
        {
            this.ans1Button.Enabled = false;
            this.ans2Button.Enabled = false;
            this.ans3Button.Enabled = false;
            this.ans4Button.Enabled = false;
            string msgToSend = ((int)msgControl.ClientServer.game.usersAnswerToQuestionGE).ToString()
                + TriviaMenu.intToByteToString(index, 1)
                + TriviaMenu.intToByteToString(this._timerLeft, 2);
            byte[] buffer = new ASCIIEncoding().GetBytes(msgToSend);
            this._stream.Write(buffer, 0, buffer.Length);
            this._stream.Flush();
            this.timer1.Stop();
            this.timeLabel.Text = _timerLeft.ToString();
            if(!(_numOfQuesDone + 1 == _numOfQuesTotal))
            {
                this._currQuesNum++;
            }
            this._numOfQuesDone++;
            this.numOfQuestionsLeftLabel.Text = "Quesions: " + this._currQuesNum.ToString()
                + "/" + this._numOfQuesTotal.ToString();
        }

        private void ans1Button_Click(object sender, EventArgs e)
        {
            sendAnsToServer(1);
            this.ans1Button.Focus();
        }

        private void ans2Button_Click(object sender, EventArgs e)
        {
            sendAnsToServer(2);
            this.ans2Button.Focus();
        }

        private void ans3Button_Click(object sender, EventArgs e)
        {
            sendAnsToServer(3);
            this.ans3Button.Focus();
        }

        private void ans4Button_Click(object sender, EventArgs e)
        {
            sendAnsToServer(4);
            this.ans4Button.Focus();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this._numOfQuesDone = this._numOfQuesTotal + 1;
            done = true;
            timer1.Stop();
            this._t.Join();
            byte[] buffer = new ASCIIEncoding().GetBytes(((int)msgControl.ClientServer.game.leaveGame).ToString());
            this._stream.Write(buffer, 0, buffer.Length);
            this._stream.Flush();
            this.Close();
        }

        private void timer_tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Invoke((MethodInvoker)delegate {
                    this._timerLeft--;
                    if (_timerLeft == 0)
                    {
                        this.timer1.Stop();
                        this._timerLeft = this._timeForQues;
                        this.sendAnsToServer(5);//ran out of time!
                    }

                    this.timeLabel.Text = _timerLeft.ToString();
                });
            }
            catch
            {
                //form closed.
            }
        }
    }
}
