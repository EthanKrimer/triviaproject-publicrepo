﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApp1
{
    public static class msgControl
    {
        /*
	    Notice!
	    if you see "GE" in the end of an enum, it is because its a 
	    general code, the rest of the msg needs to be added in the class that uses this enum
	    */
       public struct ServerClient
        {
           public enum SignInServerFeedback
            {
                GE = 102,
                Success = 0,
                WrongDetails = 1,
                UserIsAlreadyConnected = 2
            };
            public enum SignUpServerFeedback
            {
                GESignUp = 104,
                SignUpSuccess = 1040,
                PassIllegal,
                UsernameAlreadyExist,
                UsernameIllegal,
                other
            };
            public enum joiningAnExistingRoom
            {
                joiningAnExistingRoomGE = 110,
                successJoiningRoom = 1100,
                failedRoomIsFull = 1101,
                failedRoomDoesntExistOrOtherReason

            };
            public enum room
            {
                createRoomSuccess = 1140,
                createRoomFail,
                sendRoomsListGE = 106,
                sendUsersListRoomGE = 108,
                failToSendListOfUsers = 1080,
                leaveARoom = 1120,
                closeRoom = 116

            };
            public enum QuestionsAndAnswers
            {
                sendQuestionGE = 118,
                sendQuesFail = 1180,
                isAnsCorrectGE = 120
            };
            public enum gameFinished
            {
                gameFinishedGE = 121
            };
            public enum bestScores
            {
                bestScoresGE = 124
            };
            public enum selfStatus
            {
                selfStatusGE = 126
            };
        }

        public struct ClientServer
        {
            public enum signInOutUp
            {
                signInGE = 200,
                signOut,
                signUpGE = 203
            };
            public enum room
            {
                getRoomList = 205,
                getUsersListInRoomGE = 207,
                joinARoomGE = 209,
                leaveRoom = 211,
                createRoomGE = 213,
                closeRoom = 215
            };
            public enum game
            {
                startGame = 217,//sent by admin only
                usersAnswerToQuestionGE = 219,
                leaveGame = 222
            };
            public enum self
            {
                getBestScores = 223,
                getSelfStatus = 225,
                leaveApp = 299
            };
        }
    }
}
