﻿namespace WindowsFormsApp1
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.signUpScreenButton = new System.Windows.Forms.Button();
            this.signUpPasswordBox = new System.Windows.Forms.TextBox();
            this.signUpusernameBox = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.signUpPasslabel = new System.Windows.Forms.Label();
            this.signUpUsernameLabel = new System.Windows.Forms.Label();
            this.cancelSignUp = new System.Windows.Forms.Button();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.emailLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // signUpScreenButton
            // 
            this.signUpScreenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signUpScreenButton.Location = new System.Drawing.Point(456, 42);
            this.signUpScreenButton.Name = "signUpScreenButton";
            this.signUpScreenButton.Size = new System.Drawing.Size(370, 49);
            this.signUpScreenButton.TabIndex = 10;
            this.signUpScreenButton.Text = "Sign Up";
            this.signUpScreenButton.UseVisualStyleBackColor = true;
            this.signUpScreenButton.Click += new System.EventHandler(this.signUpScreenButton_Click);
            // 
            // signUpPasswordBox
            // 
            this.signUpPasswordBox.Location = new System.Drawing.Point(132, 71);
            this.signUpPasswordBox.Name = "signUpPasswordBox";
            this.signUpPasswordBox.PasswordChar = '*';
            this.signUpPasswordBox.Size = new System.Drawing.Size(318, 20);
            this.signUpPasswordBox.TabIndex = 9;
            this.signUpPasswordBox.TextChanged += new System.EventHandler(this.passwordBox_TextChanged);
            // 
            // signUpusernameBox
            // 
            this.signUpusernameBox.Location = new System.Drawing.Point(132, 42);
            this.signUpusernameBox.Name = "signUpusernameBox";
            this.signUpusernameBox.Size = new System.Drawing.Size(318, 20);
            this.signUpusernameBox.TabIndex = 8;
            this.signUpusernameBox.TextChanged += new System.EventHandler(this.usernameBox_TextChanged);
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordLabel.Location = new System.Drawing.Point(12, 71);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(100, 24);
            this.passwordLabel.TabIndex = 7;
            this.passwordLabel.Text = "Password";
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNameLabel.Location = new System.Drawing.Point(12, 38);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(114, 24);
            this.userNameLabel.TabIndex = 6;
            this.userNameLabel.Text = "User Name";
            // 
            // signUpPasslabel
            // 
            this.signUpPasslabel.AutoSize = true;
            this.signUpPasslabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.signUpPasslabel.Location = new System.Drawing.Point(149, 74);
            this.signUpPasslabel.Name = "signUpPasslabel";
            this.signUpPasslabel.Size = new System.Drawing.Size(240, 13);
            this.signUpPasslabel.TabIndex = 12;
            this.signUpPasslabel.Text = "must have 1 upper case, 1 lower case, 1 number.\r\n";
            // 
            // signUpUsernameLabel
            // 
            this.signUpUsernameLabel.AutoSize = true;
            this.signUpUsernameLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.signUpUsernameLabel.Location = new System.Drawing.Point(149, 46);
            this.signUpUsernameLabel.Name = "signUpUsernameLabel";
            this.signUpUsernameLabel.Size = new System.Drawing.Size(224, 13);
            this.signUpUsernameLabel.TabIndex = 11;
            this.signUpUsernameLabel.Text = "must have 1 english letter, no spaces allowed.";
            // 
            // cancelSignUp
            // 
            this.cancelSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cancelSignUp.Location = new System.Drawing.Point(456, 97);
            this.cancelSignUp.Name = "cancelSignUp";
            this.cancelSignUp.Size = new System.Drawing.Size(370, 49);
            this.cancelSignUp.TabIndex = 13;
            this.cancelSignUp.Text = "Cancel";
            this.cancelSignUp.UseVisualStyleBackColor = true;
            this.cancelSignUp.Click += new System.EventHandler(this.cancelSignUp_Click);
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(132, 102);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(318, 20);
            this.emailBox.TabIndex = 15;
            this.emailBox.TextChanged += new System.EventHandler(this.emailBox_TextChanged);
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.emailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.emailLabel.Location = new System.Drawing.Point(12, 102);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(62, 24);
            this.emailLabel.TabIndex = 14;
            this.emailLabel.Text = "Email";
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(844, 450);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.cancelSignUp);
            this.Controls.Add(this.signUpPasslabel);
            this.Controls.Add(this.signUpUsernameLabel);
            this.Controls.Add(this.signUpScreenButton);
            this.Controls.Add(this.signUpPasswordBox);
            this.Controls.Add(this.signUpusernameBox);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.userNameLabel);
            this.Name = "SignUp";
            this.Text = "SignUp";
            this.Load += new System.EventHandler(this.SignUp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button signUpScreenButton;
        private System.Windows.Forms.TextBox signUpPasswordBox;
        private System.Windows.Forms.TextBox signUpusernameBox;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Label signUpPasslabel;
        private System.Windows.Forms.Label signUpUsernameLabel;
        private System.Windows.Forms.Button cancelSignUp;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.Label emailLabel;
    }
}