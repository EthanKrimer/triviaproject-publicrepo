﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace WindowsFormsApp1
{
    public partial class waitingForPlayers : Form
    {
        private TriviaMenu _mainMenu;
        private NetworkStream _clientStream;
        private string _roomName;
        private string _numOfQ;
        private string _timeForQ;
        private string _roomID;        
        private List<string> _users = new List<string>();
        private Thread _receiveingT;
        static bool _isAdmin = false;

        public waitingForPlayers(TriviaMenu mainMenu, string roomName, string roomID, string details)
        {
            InitializeComponent();
            this.maxNumOfPlyrsLable.Hide();
            this.closeButton.Hide();
            this.startButton.Hide();
            this.leaveRoomButton.Hide();
            this.usersListTextBox.BackColor = System.Drawing.SystemColors.Window;
            
            this._mainMenu = mainMenu;
            this._clientStream = mainMenu._stream;
            this._roomName = roomName;
            this._roomID = roomID;

            this.usernameLable.Text = mainMenu._username;
            this.roomNameLable.Text = roomName;
            this.roomNameLable.Left = (this.ClientSize.Width - roomNameLable.Size.Width) / 2;
            if (details.Substring(0, 3) == ((int)msgControl.ClientServer.room.createRoomGE).ToString())
            {
                details = details.Remove(0, 3 + 2 + Int32.Parse(details.Substring(3, 2)));
                this.maxNumOfPlyrsLable.Show();
                this.closeButton.Show();//if its visable we are the admin
                _isAdmin = true;
                this.startButton.Show();
                this.maxNumOfPlyrsLable.Text += details.Substring(0, 1);
                details = details.Remove(0, 1);
                this._users.Add(this._mainMenu._username);
                this.usersListTextBox.Lines = this._users.ToArray();
            }
            else if (details.Substring(0, 3) == ((int)msgControl.ServerClient.joiningAnExistingRoom.joiningAnExistingRoomGE).ToString())
            {
                details = details.Remove(0, 4);
                _isAdmin = false;//we are not an admin if we have the leave room button.
                this.leaveRoomButton.Show();
            }

            this._numOfQ = details.Substring(0, 2).TrimStart('0');
            details = details.Remove(0, 2);
            this.numOfQLable.Text += this._numOfQ;

            this._timeForQ = details.Substring(0, 2).TrimStart('0');
            details = details.Remove(0, 2);
            this.timePerQLable.Text += this._timeForQ;

            this._receiveingT = new Thread(MsgReciving);
            this._receiveingT.Start();
        }

        private void MsgReciving()
        {
            int numOfUsers;
            int usernameLen;
            string rcvdM;
            byte[] bufferIn;
            int bytesRead;
            Thread.Sleep(150);
            while (true)
            {
                //receive a  message
                rcvdM = "000";
                while (rcvdM == "000")
                {
                    try
                    {
                        bufferIn = new byte[4096];
                        bytesRead = this._clientStream.Read(bufferIn, 0, 4096);
                        rcvdM = new ASCIIEncoding().GetString(bufferIn);
                    }
                    catch { }
                }

                //check which messgae received and respond
                string rcvdCode = rcvdM.Substring(0, 3);
                if (rcvdCode == ((int)msgControl.ServerClient.room.sendUsersListRoomGE).ToString())//if user list message update user list
                {
                    rcvdM = rcvdM.Remove(0, 3);
                    numOfUsers = Int32.Parse(rcvdM[0].ToString());
                    rcvdM = rcvdM.Remove(0, 1);
                    if (numOfUsers > 0)
                    {
                        this._users.Clear();
                        for (int i = 0; i < numOfUsers; i++)
                        {
                            usernameLen = Int32.Parse(rcvdM.Substring(0, 2));
                            rcvdM = rcvdM.Remove(0, 2);
                            this._users.Add(rcvdM.Substring(0, usernameLen));
                            rcvdM = rcvdM.Remove(0, usernameLen);
                        }
                        Invoke((MethodInvoker)delegate {
                            this.usersListTextBox.Text = "";
                            this.usersListTextBox.Lines = this._users.ToArray();
                        });
                    }
                }
                else if(rcvdCode == ((int)msgControl.ServerClient.QuestionsAndAnswers.sendQuestionGE).ToString())// time to start the game.
                {
                    Game game = new Game(this._mainMenu._username, this._numOfQ, this._roomName, this._timeForQ, this._mainMenu._stream, rcvdM);
                    Invoke((MethodInvoker)delegate
                    {
                        this.Hide();
                        game.ShowDialog();
                        game.Dispose();
                        this.Close();
                    });
                    return;
                }
                else if (rcvdCode == ((int)msgControl.ServerClient.room.closeRoom).ToString())//admin is closing room
                {
                    MessageBox.Show("Room is closing.");
                    if(!_isAdmin)// we are a regular user, we need to close our form.
                    {//the admin will close himself from the main thread function:: closeButton_click
                        Invoke((MethodInvoker)delegate
                        {
                            //this.Close();
                            this.closedRoomLable.Text = "The admin closed the room, press ok to continue";
                            this.closedRoomLable.Left = (this.ClientSize.Width - this.closedRoomLable.Size.Width) / 2;
                            this.leaveRoomButton.Text = "Ok";
                        });
                    }
                    return;
                }
                else if (rcvdCode == ((int)msgControl.ServerClient.room.leaveARoom).ToString().Substring(0,3))//is leaving room
                {
                    if(rcvdM.Substring(3,1) == "0")
                    {
                        //we can leave
                        return;
                    }
                }
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //send close room message
            string msg = ((int)msgControl.ClientServer.room.closeRoom).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(msg);
            this._clientStream.Write(buffer, 0, buffer.Length);
            this._clientStream.Flush();
            
            //wait for answer
            this._receiveingT.Join();

            this.Close();
        }

        private void leaveRoomButton_Click(object sender, EventArgs e)
        {
            if (this.leaveRoomButton.Text == "Leave Room")//if room is open
            {
                //send leave room message
                string msg = ((int)msgControl.ClientServer.room.leaveRoom).ToString();
                byte[] buffer = new ASCIIEncoding().GetBytes(msg);
                this._clientStream.Write(buffer, 0, buffer.Length);
                this._clientStream.Flush();
                
                //wait for answer
                this._receiveingT.Join();
            }
            this.Close();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            string msg = ((int)msgControl.ClientServer.game.startGame).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(msg);
            this._clientStream.Write(buffer, 0, buffer.Length);
            this._clientStream.Flush();
            //from this point the side thread will handle the rest.
        }
    }
}
