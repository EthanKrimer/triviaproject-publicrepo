﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class personalStatusMenu : Form
    {
        public personalStatusMenu(string username, string personalStatus)
        {
            InitializeComponent();

            this.label6.Text = username;
            this.label2.Text += ParseStr(personalStatus.Substring(0, 4).TrimStart('0'));
            this.label3.Text += ParseStr(personalStatus.Substring(4, 6).TrimStart('0'));
            this.label4.Text += ParseStr(personalStatus.Substring(10, 6).TrimStart('0'));
            this.label5.Text += ParseStr(personalStatus.Substring(16, 2).TrimStart('0')) + "." + personalStatus.Substring(18, 2);
        }

        private string ParseStr(string toParse)
        {
            return toParse.Length > 0 ? " " + toParse : " 0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
