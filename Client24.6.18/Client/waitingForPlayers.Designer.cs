﻿namespace WindowsFormsApp1
{
    partial class waitingForPlayers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameLable = new System.Windows.Forms.Label();
            this.roomNameLable = new System.Windows.Forms.Label();
            this.maxNumOfPlyrsLable = new System.Windows.Forms.Label();
            this.numOfQLable = new System.Windows.Forms.Label();
            this.timePerQLable = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.usersListTextBox = new System.Windows.Forms.RichTextBox();
            this.leaveRoomButton = new System.Windows.Forms.Button();
            this.closedRoomLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // usernameLable
            // 
            this.usernameLable.AutoSize = true;
            this.usernameLable.Location = new System.Drawing.Point(12, 9);
            this.usernameLable.Name = "usernameLable";
            this.usernameLable.Size = new System.Drawing.Size(0, 13);
            this.usernameLable.TabIndex = 0;
            // 
            // roomNameLable
            // 
            this.roomNameLable.AutoSize = true;
            this.roomNameLable.Location = new System.Drawing.Point(12, 9);
            this.roomNameLable.Name = "roomNameLable";
            this.roomNameLable.Size = new System.Drawing.Size(0, 13);
            this.roomNameLable.TabIndex = 1;
            // 
            // maxNumOfPlyrsLable
            // 
            this.maxNumOfPlyrsLable.AutoSize = true;
            this.maxNumOfPlyrsLable.Location = new System.Drawing.Point(12, 31);
            this.maxNumOfPlyrsLable.Margin = new System.Windows.Forms.Padding(3);
            this.maxNumOfPlyrsLable.Name = "maxNumOfPlyrsLable";
            this.maxNumOfPlyrsLable.Size = new System.Drawing.Size(124, 13);
            this.maxNumOfPlyrsLable.TabIndex = 2;
            this.maxNumOfPlyrsLable.Text = "Max Number Of Players: ";
            // 
            // numOfQLable
            // 
            this.numOfQLable.AutoSize = true;
            this.numOfQLable.Location = new System.Drawing.Point(12, 50);
            this.numOfQLable.Margin = new System.Windows.Forms.Padding(3);
            this.numOfQLable.Name = "numOfQLable";
            this.numOfQLable.Size = new System.Drawing.Size(114, 13);
            this.numOfQLable.TabIndex = 3;
            this.numOfQLable.Text = "Number Of Questions: ";
            // 
            // timePerQLable
            // 
            this.timePerQLable.AutoSize = true;
            this.timePerQLable.Location = new System.Drawing.Point(12, 69);
            this.timePerQLable.Margin = new System.Windows.Forms.Padding(3);
            this.timePerQLable.Name = "timePerQLable";
            this.timePerQLable.Size = new System.Drawing.Size(100, 13);
            this.timePerQLable.TabIndex = 4;
            this.timePerQLable.Text = "Time Per Question: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 92);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Current Participants Are:";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(117, 214);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 7;
            this.closeButton.Text = "Close Room";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(104, 243);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(100, 50);
            this.startButton.TabIndex = 8;
            this.startButton.Text = "Start Game";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // usersListTextBox
            // 
            this.usersListTextBox.Location = new System.Drawing.Point(104, 112);
            this.usersListTextBox.Name = "usersListTextBox";
            this.usersListTextBox.ReadOnly = true;
            this.usersListTextBox.Size = new System.Drawing.Size(100, 96);
            this.usersListTextBox.TabIndex = 10;
            this.usersListTextBox.Text = "";
            // 
            // leaveRoomButton
            // 
            this.leaveRoomButton.Location = new System.Drawing.Point(104, 243);
            this.leaveRoomButton.Name = "leaveRoomButton";
            this.leaveRoomButton.Size = new System.Drawing.Size(100, 50);
            this.leaveRoomButton.TabIndex = 11;
            this.leaveRoomButton.Text = "Leave Room";
            this.leaveRoomButton.UseVisualStyleBackColor = true;
            this.leaveRoomButton.Click += new System.EventHandler(this.leaveRoomButton_Click);
            // 
            // closedRoomLable
            // 
            this.closedRoomLable.AutoSize = true;
            this.closedRoomLable.Location = new System.Drawing.Point(101, 219);
            this.closedRoomLable.Name = "closedRoomLable";
            this.closedRoomLable.Size = new System.Drawing.Size(0, 13);
            this.closedRoomLable.TabIndex = 12;
            // 
            // waitingForPlayers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 302);
            this.Controls.Add(this.closedRoomLable);
            this.Controls.Add(this.leaveRoomButton);
            this.Controls.Add(this.usersListTextBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.timePerQLable);
            this.Controls.Add(this.numOfQLable);
            this.Controls.Add(this.maxNumOfPlyrsLable);
            this.Controls.Add(this.roomNameLable);
            this.Controls.Add(this.usernameLable);
            this.Name = "waitingForPlayers";
            this.Text = "waitingForPlayers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernameLable;
        private System.Windows.Forms.Label roomNameLable;
        private System.Windows.Forms.Label maxNumOfPlyrsLable;
        private System.Windows.Forms.Label numOfQLable;
        private System.Windows.Forms.Label timePerQLable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.RichTextBox usersListTextBox;
        private System.Windows.Forms.Button leaveRoomButton;
        private System.Windows.Forms.Label closedRoomLable;
    }
}