﻿namespace WindowsFormsApp1
{
    partial class RoomMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomList = new System.Windows.Forms.ListBox();
            this.usersList = new System.Windows.Forms.ListBox();
            this.RoomsLabel = new System.Windows.Forms.Label();
            this.UsersLabel = new System.Windows.Forms.Label();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.joinButton = new System.Windows.Forms.Button();
            this.ErrorMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // roomList
            // 
            this.roomList.FormattingEnabled = true;
            this.roomList.Location = new System.Drawing.Point(12, 34);
            this.roomList.Name = "roomList";
            this.roomList.Size = new System.Drawing.Size(260, 108);
            this.roomList.TabIndex = 0;
            this.roomList.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // usersList
            // 
            this.usersList.FormattingEnabled = true;
            this.usersList.Location = new System.Drawing.Point(12, 168);
            this.usersList.Name = "usersList";
            this.usersList.Size = new System.Drawing.Size(260, 108);
            this.usersList.TabIndex = 1;
            this.usersList.SelectedIndexChanged += new System.EventHandler(this.usersList_SelectedIndexChanged);
            // 
            // RoomsLabel
            // 
            this.RoomsLabel.AutoSize = true;
            this.RoomsLabel.Location = new System.Drawing.Point(100, 9);
            this.RoomsLabel.Name = "RoomsLabel";
            this.RoomsLabel.Size = new System.Drawing.Size(58, 13);
            this.RoomsLabel.TabIndex = 2;
            this.RoomsLabel.Text = "Rooms list:";
            // 
            // UsersLabel
            // 
            this.UsersLabel.AutoSize = true;
            this.UsersLabel.Location = new System.Drawing.Point(100, 152);
            this.UsersLabel.Name = "UsersLabel";
            this.UsersLabel.Size = new System.Drawing.Size(52, 13);
            this.UsersLabel.TabIndex = 3;
            this.UsersLabel.Text = "Users list:";
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(12, 282);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(75, 23);
            this.RefreshButton.TabIndex = 4;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // joinButton
            // 
            this.joinButton.Location = new System.Drawing.Point(197, 282);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(75, 23);
            this.joinButton.TabIndex = 5;
            this.joinButton.Text = "Join Room";
            this.joinButton.UseVisualStyleBackColor = true;
            this.joinButton.Click += new System.EventHandler(this.joinButton_Click);
            // 
            // ErrorMsg
            // 
            this.ErrorMsg.AutoSize = true;
            this.ErrorMsg.Location = new System.Drawing.Point(100, 332);
            this.ErrorMsg.Name = "ErrorMsg";
            this.ErrorMsg.Size = new System.Drawing.Size(35, 13);
            this.ErrorMsg.TabIndex = 6;
            this.ErrorMsg.Text = "label1";
            // 
            // RoomMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 366);
            this.Controls.Add(this.ErrorMsg);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.UsersLabel);
            this.Controls.Add(this.RoomsLabel);
            this.Controls.Add(this.usersList);
            this.Controls.Add(this.roomList);
            this.Name = "RoomMenu";
            this.Text = "RoomMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox roomList;
        private System.Windows.Forms.ListBox usersList;
        private System.Windows.Forms.Label RoomsLabel;
        private System.Windows.Forms.Label UsersLabel;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.Label ErrorMsg;
    }
}