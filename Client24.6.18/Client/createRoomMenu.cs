﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{

    public partial class createRoomMenu : Form
    {
        private TriviaMenu _TriviaMenu;

        public createRoomMenu(TriviaMenu triviaMenu)
        {
            InitializeComponent();
            this.usernameLable.Text = triviaMenu._username;
            this._TriviaMenu = triviaMenu;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            if (this.roomNameTB.Text != "")
            {
                string roomCreationMsg =
                    ((int)msgControl.ClientServer.room.createRoomGE).ToString() +
                    TriviaMenu.intToByteToString(this.roomNameTB.Text.Length, 2) + this.roomNameTB.Text +
                    this.numOfPNUD.Value.ToString() +
                    TriviaMenu.intToByteToString((int)this.numOfQNUD.Value, 2) +
                    TriviaMenu.intToByteToString((int)this.timeForQNUD.Value, 2);
                byte[] buffer = new ASCIIEncoding().GetBytes(roomCreationMsg);
                this._TriviaMenu._stream.Write(buffer, 0, buffer.Length);
                this._TriviaMenu._stream.Flush();
                string rcvdM = "";
                do {
                    try
                    {
                        byte[] bufferIn = new byte[4];
                        int bytesRead = this._TriviaMenu._stream.Read(bufferIn, 0, 4);
                        rcvdM = new ASCIIEncoding().GetString(bufferIn);
                    }
                    catch { }
                } while (rcvdM != ((int)msgControl.ServerClient.room.createRoomSuccess).ToString());
                waitingForPlayers menu = new waitingForPlayers(this._TriviaMenu,this.roomNameTB.Text,"", roomCreationMsg);
                this.Hide();
                menu.ShowDialog();
                menu.Dispose();
                this.Close();
            }
            else
            {
                this.errorMsg.Text = "Illegal Room Name";
            }
        }
    }
}
