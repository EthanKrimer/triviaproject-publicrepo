﻿namespace WindowsFormsApp1
{
    partial class createRoomMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameLable = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.roomNameTB = new System.Windows.Forms.TextBox();
            this.roomName = new System.Windows.Forms.Label();
            this.numOfP = new System.Windows.Forms.Label();
            this.numOfQ = new System.Windows.Forms.Label();
            this.timeForQ = new System.Windows.Forms.Label();
            this.sendButton = new System.Windows.Forms.Button();
            this.numOfPNUD = new System.Windows.Forms.NumericUpDown();
            this.numOfQNUD = new System.Windows.Forms.NumericUpDown();
            this.timeForQNUD = new System.Windows.Forms.NumericUpDown();
            this.errorMsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numOfPNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfQNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeForQNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameLable
            // 
            this.usernameLable.AutoSize = true;
            this.usernameLable.Location = new System.Drawing.Point(12, 17);
            this.usernameLable.Name = "usernameLable";
            this.usernameLable.Size = new System.Drawing.Size(0, 13);
            this.usernameLable.TabIndex = 0;
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(154, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 1;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // roomNameTB
            // 
            this.roomNameTB.Location = new System.Drawing.Point(129, 52);
            this.roomNameTB.MaxLength = 99;
            this.roomNameTB.Name = "roomNameTB";
            this.roomNameTB.Size = new System.Drawing.Size(100, 20);
            this.roomNameTB.TabIndex = 2;
            this.roomNameTB.Text = "Room_Name";
            // 
            // roomName
            // 
            this.roomName.AutoSize = true;
            this.roomName.Location = new System.Drawing.Point(12, 55);
            this.roomName.Name = "roomName";
            this.roomName.Size = new System.Drawing.Size(69, 13);
            this.roomName.TabIndex = 3;
            this.roomName.Text = "Room Name:";
            // 
            // numOfP
            // 
            this.numOfP.AutoSize = true;
            this.numOfP.Location = new System.Drawing.Point(12, 81);
            this.numOfP.Name = "numOfP";
            this.numOfP.Size = new System.Drawing.Size(98, 13);
            this.numOfP.TabIndex = 4;
            this.numOfP.Text = "Number Of Players:";
            // 
            // numOfQ
            // 
            this.numOfQ.AutoSize = true;
            this.numOfQ.Location = new System.Drawing.Point(12, 107);
            this.numOfQ.Name = "numOfQ";
            this.numOfQ.Size = new System.Drawing.Size(111, 13);
            this.numOfQ.TabIndex = 7;
            this.numOfQ.Text = "Number Of Questions:";
            // 
            // timeForQ
            // 
            this.timeForQ.AutoSize = true;
            this.timeForQ.Location = new System.Drawing.Point(12, 133);
            this.timeForQ.Name = "timeForQ";
            this.timeForQ.Size = new System.Drawing.Size(96, 13);
            this.timeForQ.TabIndex = 9;
            this.timeForQ.Text = "Time For Question:";
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(148, 177);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 10;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // numOfPNUD
            // 
            this.numOfPNUD.Location = new System.Drawing.Point(129, 79);
            this.numOfPNUD.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numOfPNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfPNUD.Name = "numOfPNUD";
            this.numOfPNUD.Size = new System.Drawing.Size(100, 20);
            this.numOfPNUD.TabIndex = 11;
            this.numOfPNUD.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // numOfQNUD
            // 
            this.numOfQNUD.Location = new System.Drawing.Point(129, 105);
            this.numOfQNUD.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numOfQNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfQNUD.Name = "numOfQNUD";
            this.numOfQNUD.Size = new System.Drawing.Size(100, 20);
            this.numOfQNUD.TabIndex = 12;
            this.numOfQNUD.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // timeForQNUD
            // 
            this.timeForQNUD.Location = new System.Drawing.Point(129, 131);
            this.timeForQNUD.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.timeForQNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.timeForQNUD.Name = "timeForQNUD";
            this.timeForQNUD.Size = new System.Drawing.Size(100, 20);
            this.timeForQNUD.TabIndex = 13;
            this.timeForQNUD.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // errorMsg
            // 
            this.errorMsg.AutoSize = true;
            this.errorMsg.Location = new System.Drawing.Point(12, 182);
            this.errorMsg.Name = "errorMsg";
            this.errorMsg.Size = new System.Drawing.Size(0, 13);
            this.errorMsg.TabIndex = 14;
            // 
            // createRoomMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 212);
            this.Controls.Add(this.errorMsg);
            this.Controls.Add(this.timeForQNUD);
            this.Controls.Add(this.numOfQNUD);
            this.Controls.Add(this.numOfPNUD);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.timeForQ);
            this.Controls.Add(this.numOfQ);
            this.Controls.Add(this.numOfP);
            this.Controls.Add(this.roomName);
            this.Controls.Add(this.roomNameTB);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.usernameLable);
            this.Name = "createRoomMenu";
            this.Text = "createRoomMenu";
            ((System.ComponentModel.ISupportInitialize)(this.numOfPNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfQNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeForQNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernameLable;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.TextBox roomNameTB;
        private System.Windows.Forms.Label roomName;
        private System.Windows.Forms.Label numOfP;
        private System.Windows.Forms.Label numOfQ;
        private System.Windows.Forms.Label timeForQ;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.NumericUpDown numOfPNUD;
        private System.Windows.Forms.NumericUpDown numOfQNUD;
        private System.Windows.Forms.NumericUpDown timeForQNUD;
        private System.Windows.Forms.Label errorMsg;
    }
}