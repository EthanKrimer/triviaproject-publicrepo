﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class bestScoresMenu : Form
    {
        public bestScoresMenu(string username, string msg)
        {
            InitializeComponent();

            this.label1.Text = username;
            for (int i = 0; i < 3; i++)
            {
                int usernameLen = Int32.Parse(msg.Substring(0, 2).TrimStart('0'));
                msg = msg.Remove(0, 2);
                this.Controls["label" + (i + 3).ToString()].Text = msg.Substring(0, usernameLen);
                msg = msg.Remove(0, usernameLen);
                this.Controls["label" + (i + 3).ToString()].Text += " - " + msg.Substring(0, 6).TrimStart('0');
                msg = msg.Remove(0, 6);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
