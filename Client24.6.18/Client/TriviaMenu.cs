﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApp1
{
    public partial class TriviaMenu : Form
    {
		private int _port = 8643;
        private string _ip = "127.0.0.1";
        public string _msg = "";
        public string _rcvMsg = "";
        public string _username = "";
        private bool _quit = false;
        public NetworkStream _stream;

        public TriviaMenu()
        {
            InitializeComponent();
            this.Hide();
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@"config.txt");
                string[] ip = lines[0].Split('=');
                string[] port = lines[1].Split('=');
                this._port = Int32.Parse(port[1]);
                this._ip = ip[1];
            }
            catch
            {
                MessageBox.Show("Reading from config file failed, using default values for ip and port.");
            }
            Thread myT = new Thread(connectToServer);
            myT.Start();

        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            this._quit = true;
            Thread.Sleep(150);
            Environment.Exit(0);
            e.Cancel = false;
            base.OnFormClosing(e);
        }
        private void signInButton_Click(object sender, EventArgs e)
        {
            string username = this.usernameBox.Text;
            string password = this.passwordBox.Text;
            if (username == "" || password == "")
                return;
            this._username = username;
            this._msg = ((int)msgControl.ClientServer.signInOutUp.signInGE).ToString() + intToByteToString(username.Length, 2) + username + intToByteToString(password.Length, 2) + password;
        }

        public static string intToByteToString(int myInt, int numOfWantedBytes)
        {
            string s = myInt.ToString();
            return s.PadLeft(numOfWantedBytes, '0');
        }

        //side thread that connects to the server and sends msgs
        private void connectToServer()
        {
            Thread.Sleep(100);
            //establish connection:
            TcpClient client = new TcpClient();
            NetworkStream clientStream;
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(this._ip), this._port);
            ConnectToServer:
            try
            {
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();
				this._stream = clientStream;
                Invoke((MethodInvoker)delegate
                {
                    this.Show(); this.WelcomeMsgLabel.Hide(); this.joinroomButton.Enabled = false;
                    this.createroomButton.Enabled = false; this.mystatusButton.Enabled = false; this.errorSignInLabel.Hide();
                    this.bestscoresButton.Enabled = false;
                });
                client.ReceiveTimeout = 150;
                while (!this._quit)
                {
                    if (this._msg != "")
                    {
                        byte[] buffer = new ASCIIEncoding().GetBytes(this._msg);
                        clientStream.Write(buffer, 0, buffer.Length);
                        clientStream.Flush();
                        this._msg = "";
                    }
                    try
                    {
                        byte[] bufferIn = new byte[3];
                        int bytesRead = clientStream.Read(bufferIn, 0, 3);
                        this._rcvMsg = new ASCIIEncoding().GetString(bufferIn);
                    }
                    catch
                    {
                        //didnt get anything, continue.
                    }
                    try
                    {
                        if (this._rcvMsg != "")
                        {
                            int code = Int32.Parse(this._rcvMsg);
                            manage(code, clientStream);
                            this._rcvMsg = "";
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Exception!\n" + e.ToString());
                    }
                }
            }
            catch
            {
                Invoke((MethodInvoker)delegate { this.Hide(); });
                DialogResult dialogResult = MessageBox.Show("There was an error while attempting to connect to TriviaServer.exe\n"
                    + "Please make sure that you are running a copy of TriviaServer.exe\n Would you like to try connecting again?",
                    "Attention!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    goto ConnectToServer;
                }
                else if (dialogResult == DialogResult.No)
                {
                    //lets finish.
                    Environment.Exit(0);
                }
            }

        }
        private void handleSignIn(string ack)
        {
            int iAck = Int32.Parse(ack);
            switch (iAck)
            {
                case 0://sign in successful
                    Invoke((MethodInvoker)delegate {
                        this.WelcomeMsgLabel.Text = "Hello, " + this._username; this.WelcomeMsgLabel.Show();
                        this.signInButton.Hide(); this.usernameBox.Hide(); this.userNameLabel.Hide(); this.passwordBox.Hide();
                        this.passwordLabel.Hide(); this.signupOrOutButton.Text = "Sign Out"; this.joinroomButton.Enabled = true;
                        this.createroomButton.Enabled = true; this.mystatusButton.Enabled = true; this.bestscoresButton.Enabled = true;
                        this.errorSignInLabel.Hide();
                    });
                    break;
                case 1://wrong details
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Text = "Wrong details!";
                        this._username = "";
                        this.errorSignInLabel.Show();
                    });
                    break;
                case 2://user already connected
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Text = "User already connected!";
                        this._username = "";
                        this.errorSignInLabel.Show();
                    });
                    break;
            }
        }
        public void manage(int code, NetworkStream clientStream)
        {

            byte[] bufferIn;
            int bytesRead;
            switch (code)
            {
                case (int)msgControl.ServerClient.SignInServerFeedback.GE:
                    bufferIn = new byte[1];
                    bytesRead = clientStream.Read(bufferIn, 0, 1);
                    handleSignIn(new ASCIIEncoding().GetString(bufferIn));
                    break;

                case (int)msgControl.ServerClient.SignUpServerFeedback.GESignUp:
                    bufferIn = new byte[1];
                    bytesRead = clientStream.Read(bufferIn, 0, 1);
                    handleSignUp(new ASCIIEncoding().GetString(bufferIn));
                    break;

                case (int)msgControl.ServerClient.room.sendRoomsListGE:
                    bufferIn = new byte[4096];
                    bytesRead = clientStream.Read(bufferIn, 0, 4096);
                    List<string> list = GetRoomsList(new ASCIIEncoding().GetString(bufferIn));
                    Invoke((MethodInvoker)delegate {
                        RoomMenu roomsMenu = new RoomMenu(list, this, clientStream);
                        this.Hide(); roomsMenu.ShowDialog();
                        roomsMenu.Dispose();
                        this.Show();
                    });
                    break;
				
				case (int)msgControl.ServerClient.selfStatus.selfStatusGE:
                    bufferIn = new byte[4096];
                    bytesRead = clientStream.Read(bufferIn, 0, 4096);
                    handlePersonalStatus(new ASCIIEncoding().GetString(bufferIn));
                    break;
 
                case (int)msgControl.ServerClient.bestScores.bestScoresGE:
                    bufferIn = new byte[4096];
                    bytesRead = clientStream.Read(bufferIn, 0, 4096);
                    HandleBestScores(new ASCIIEncoding().GetString(bufferIn));
                    break;
				
				case (int)msgControl.ServerClient.room.createRoomSuccess:
                    createRoomMenu menu = new createRoomMenu(this);
                    Invoke((MethodInvoker)delegate {
                        this.Hide();
                        menu.ShowDialog();
                        menu.Dispose();
                        this.Show();
                    });
                    break;
            }

        }

        public List<string> GetRoomsList(string v)
        {
            string numOfRooms = v.Substring(0, 4).TrimStart('0');
            v = v.Remove(0, 4);
            List<string> list = new List<string>();
            int numOfRoomsInt = 0;
            if (numOfRooms != "")
            {
                numOfRoomsInt = Int32.Parse(numOfRooms);
                for (int i = 0; i < numOfRoomsInt; i++)
                {
                    list.Add(v.Substring(0, 4).TrimStart('0'));//adding the id
                    v = v.Remove(0,4);
                    int length = Int32.Parse(v.Substring(0, 2).TrimStart('0'));
                    v = v.Remove(0, 2);
                    list.Add(v.Substring(0, length));//adding the name
                    v = v.Remove(0, length);
                }
            }
            return list;
        }

        private void handleSignUp(string v)
        {
            int iAck = Int32.Parse(v);
            switch (iAck)
            {
                case 0://sign up successful
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Show();
                        this.errorSignInLabel.Text = "Sign Up successful";//we'll use this label to let the user know
                    });
                    break;
                case 1://pass illegal
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Show();
                        this.errorSignInLabel.Text = "Pass Illegal";
                    });
                    break;
                case 2://user already exist
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Show();
                        this.errorSignInLabel.Text = "User already exists";
                    });
                    break;
                case 3://username illegal
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Show();
                        this.errorSignInLabel.Text = "username Illegal";
                    });
                    break;
                case 4://other
                    Invoke((MethodInvoker)delegate {
                        this.errorSignInLabel.Show();
                        this.errorSignInLabel.Text = "lol Idk, something wrong happend";
                    });
                    break;
            }
        }

        private void signupOrOutButton_Click(object sender, EventArgs e)
        {
            if(this.signupOrOutButton.Text == "Sign Out")
            {
                Invoke((MethodInvoker)delegate {
                    this._username = ""; this.WelcomeMsgLabel.Hide();
                    this.signInButton.Show(); this.usernameBox.Show(); this.userNameLabel.Show(); this.passwordBox.Show();
                    this.passwordLabel.Show(); this.signupOrOutButton.Text = "Sign Up"; this.joinroomButton.Enabled = false;
                    this.createroomButton.Enabled = false; this.mystatusButton.Enabled = false; this.bestscoresButton.Enabled = false;
                    this.errorSignInLabel.Hide();
                });
                this._msg = ((int)msgControl.ClientServer.signInOutUp.signOut).ToString();
                return;
            }
            if(this.signupOrOutButton.Text == "Sign Up")
            {
                SignUp signUp = new SignUp(this);
                this.Hide();
                signUp.ShowDialog();
                signUp.Dispose();
                this.Show();
            }
        }

		private void handlePersonalStatus(string rcvMsg)
        {
            personalStatusMenu menu = new personalStatusMenu(this._username, rcvMsg);
            Invoke((MethodInvoker)delegate {
                this.Hide();
                menu.ShowDialog();
                menu.Dispose();
                this.Show();
            });
        }
 
 
        private void TriviaMenu_Load(object sender, EventArgs e)
        {
 
        }
 
        private void HandleBestScores(string rcvMsg)
        {
            bestScoresMenu menu = new bestScoresMenu(this._username, rcvMsg);
            Invoke((MethodInvoker)delegate {
                this.Hide();
                menu.ShowDialog();
                menu.Dispose();
                this.Show();
            });
        }
 
 
        private void joinroomButton_Click(object sender, EventArgs e)
        {
            this._msg = ((int)msgControl.ClientServer.room.getRoomList).ToString();
        }

        private void bestscoresButton_Click_1(object sender, EventArgs e)
        {
            if (this._username != "") this._msg = ((int)msgControl.ClientServer.self.getBestScores).ToString();
        }

        private void mystatusButton_Click_1(object sender, EventArgs e)
        {
            if (this._username != "") this._msg = ((int)msgControl.ClientServer.self.getSelfStatus).ToString();
        }

        private void createroomButton_Click_1(object sender, EventArgs e)
        {
            if (this._username != "")
            {
                this._rcvMsg = ((int)msgControl.ServerClient.room.createRoomSuccess).ToString();
            }
        }

        private void quitButton_Click_1(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
