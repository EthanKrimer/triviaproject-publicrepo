﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class SignUp : Form
    {
        private TriviaMenu _TriviaMenu;
        public SignUp(TriviaMenu triviaMenu)
        {
            InitializeComponent();
            this._TriviaMenu = triviaMenu;
        }

        private void usernameBox_TextChanged(object sender, EventArgs e)
        {
            this.signUpUsernameLabel.Hide();
            if(this.signUpusernameBox.Text == "")
                this.signUpUsernameLabel.Show();
        }

        private void passwordBox_TextChanged(object sender, EventArgs e)
        {
            this.signUpPasslabel.Hide();
            if(this.signUpPasswordBox.Text == "")
                this.signUpPasslabel.Show();
        }

        private void SignUp_Load(object sender, EventArgs e)
        {
            this.signUpPasslabel.Show();
            this.signUpUsernameLabel.Show();
        }

        private void signUpScreenButton_Click(object sender, EventArgs e)
        {
            string username = this.signUpusernameBox.Text;
            string password = this.signUpPasswordBox.Text;
            string email = this.emailBox.Text;
            if(username == "" || password == "" || email == "")
                return;
            while (this._TriviaMenu._msg != "") ;
            this._TriviaMenu._msg = ((int)msgControl.ClientServer.signInOutUp.signUpGE).ToString() + 
                TriviaMenu.intToByteToString(username.Length,2) + username +
                TriviaMenu.intToByteToString(password.Length,2) + password +
                TriviaMenu.intToByteToString(email.Length, 2) + email;
            this.Close();
        }

        private void cancelSignUp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void emailBox_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
