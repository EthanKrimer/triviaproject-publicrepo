﻿namespace WindowsFormsApp1
{
    partial class TriviaMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userNameLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.signInButton = new System.Windows.Forms.Button();
            this.signupOrOutButton = new System.Windows.Forms.Button();
            this.joinroomButton = new System.Windows.Forms.Button();
            this.createroomButton = new System.Windows.Forms.Button();
            this.mystatusButton = new System.Windows.Forms.Button();
            this.bestscoresButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.WelcomeMsgLabel = new System.Windows.Forms.Label();
            this.errorSignInLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNameLabel.Location = new System.Drawing.Point(32, 113);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(114, 24);
            this.userNameLabel.TabIndex = 1;
            this.userNameLabel.Text = "User Name";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordLabel.Location = new System.Drawing.Point(32, 146);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(100, 24);
            this.passwordLabel.TabIndex = 2;
            this.passwordLabel.Text = "Password";
            // 
            // usernameBox
            // 
            this.usernameBox.Location = new System.Drawing.Point(152, 117);
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(318, 20);
            this.usernameBox.TabIndex = 3;
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(152, 146);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.PasswordChar = '*';
            this.passwordBox.Size = new System.Drawing.Size(318, 20);
            this.passwordBox.TabIndex = 4;
            // 
            // signInButton
            // 
            this.signInButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signInButton.Location = new System.Drawing.Point(476, 117);
            this.signInButton.Name = "signInButton";
            this.signInButton.Size = new System.Drawing.Size(370, 49);
            this.signInButton.TabIndex = 5;
            this.signInButton.Text = "Sign In";
            this.signInButton.UseVisualStyleBackColor = true;
            this.signInButton.Click += new System.EventHandler(this.signInButton_Click);
            // 
            // signupOrOutButton
            // 
            this.signupOrOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signupOrOutButton.Location = new System.Drawing.Point(237, 210);
            this.signupOrOutButton.Name = "signupOrOutButton";
            this.signupOrOutButton.Size = new System.Drawing.Size(384, 63);
            this.signupOrOutButton.TabIndex = 8;
            this.signupOrOutButton.Text = "Sign Up";
            this.signupOrOutButton.UseVisualStyleBackColor = true;
            this.signupOrOutButton.Click += new System.EventHandler(this.signupOrOutButton_Click);
            // 
            // joinroomButton
            // 
            this.joinroomButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.joinroomButton.Location = new System.Drawing.Point(237, 279);
            this.joinroomButton.Name = "joinroomButton";
            this.joinroomButton.Size = new System.Drawing.Size(384, 63);
            this.joinroomButton.TabIndex = 9;
            this.joinroomButton.Text = "Join Room";
            this.joinroomButton.UseVisualStyleBackColor = true;
            this.joinroomButton.Click += new System.EventHandler(this.joinroomButton_Click);
            // 
            // createroomButton
            // 
            this.createroomButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.createroomButton.Location = new System.Drawing.Point(237, 348);
            this.createroomButton.Name = "createroomButton";
            this.createroomButton.Size = new System.Drawing.Size(384, 63);
            this.createroomButton.TabIndex = 10;
            this.createroomButton.Text = "Create Room";
            this.createroomButton.UseVisualStyleBackColor = true;
            this.createroomButton.Click += new System.EventHandler(this.createroomButton_Click_1);
            // 
            // mystatusButton
            // 
            this.mystatusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.mystatusButton.Location = new System.Drawing.Point(237, 417);
            this.mystatusButton.Name = "mystatusButton";
            this.mystatusButton.Size = new System.Drawing.Size(384, 63);
            this.mystatusButton.TabIndex = 11;
            this.mystatusButton.Text = "My Status";
            this.mystatusButton.UseVisualStyleBackColor = true;
            this.mystatusButton.Click += new System.EventHandler(this.mystatusButton_Click_1);
            // 
            // bestscoresButton
            // 
            this.bestscoresButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.bestscoresButton.Location = new System.Drawing.Point(237, 486);
            this.bestscoresButton.Name = "bestscoresButton";
            this.bestscoresButton.Size = new System.Drawing.Size(384, 63);
            this.bestscoresButton.TabIndex = 12;
            this.bestscoresButton.Text = "Best Scores";
            this.bestscoresButton.UseVisualStyleBackColor = true;
            this.bestscoresButton.Click += new System.EventHandler(this.bestscoresButton_Click_1);
            // 
            // quitButton
            // 
            this.quitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quitButton.Location = new System.Drawing.Point(388, 750);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(82, 23);
            this.quitButton.TabIndex = 17;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click_1);
            // 
            // WelcomeMsgLabel
            // 
            this.WelcomeMsgLabel.AutoSize = true;
            this.WelcomeMsgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.WelcomeMsgLabel.Location = new System.Drawing.Point(323, 121);
            this.WelcomeMsgLabel.Name = "WelcomeMsgLabel";
            this.WelcomeMsgLabel.Size = new System.Drawing.Size(158, 55);
            this.WelcomeMsgLabel.TabIndex = 18;
            this.WelcomeMsgLabel.Text = "label1";
            // 
            // errorSignInLabel
            // 
            this.errorSignInLabel.AutoSize = true;
            this.errorSignInLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.errorSignInLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.errorSignInLabel.Location = new System.Drawing.Point(506, 173);
            this.errorSignInLabel.Name = "errorSignInLabel";
            this.errorSignInLabel.Size = new System.Drawing.Size(57, 20);
            this.errorSignInLabel.TabIndex = 19;
            this.errorSignInLabel.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(731, 617);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 26);
            this.label1.TabIndex = 20;
            this.label1.Text = "A simple trivia client,\r\nBy Max and Ethan.\r\n";
            // 
            // TriviaMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(858, 802);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.errorSignInLabel);
            this.Controls.Add(this.WelcomeMsgLabel);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.bestscoresButton);
            this.Controls.Add(this.mystatusButton);
            this.Controls.Add(this.createroomButton);
            this.Controls.Add(this.joinroomButton);
            this.Controls.Add(this.signupOrOutButton);
            this.Controls.Add(this.signInButton);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.usernameBox);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.userNameLabel);
            this.Name = "TriviaMenu";
            this.Text = "Simple Client for Trivia game, haha yes.";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox usernameBox;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.Button signInButton;
        private System.Windows.Forms.Button signupOrOutButton;
        private System.Windows.Forms.Button joinroomButton;
        private System.Windows.Forms.Button createroomButton;
        private System.Windows.Forms.Button mystatusButton;
        private System.Windows.Forms.Button bestscoresButton;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.Label WelcomeMsgLabel;
        private System.Windows.Forms.Label errorSignInLabel;
        private System.Windows.Forms.Label label1;
    }
}